using System;
using System.Collections.Generic;
using Project_B_Cinema.Datahandler;

namespace Project_B_Cinema.Classes

{
    public class Reservation
    {
        public int id { get; set; }
        public DateTime filmViewingTime { get; set; }
        public string filmName { get; set; }
        public User user { get; set; }
        public Tuple<double, List<string>, List<double>> snacks { get; set; }
        public List<int> seats {get; set; }
        public double seatPrice { get; set; }
        public string printReservation(){
            string s = "---------------------------------------\n";
            s += ($"Film: {this.filmName}\n");
            s += ($"Tijd: {this.filmViewingTime}\n");
            double sum = 0.0;
            int i = 0;
            if (this.snacks.Item2.Count > 0)
            {
                s += "\nConsumpties:\n";
                sum += this.snacks.Item1;
                foreach (string food in this.snacks.Item2)
                {
                    s += food + "\n";
                    i++;
                }
                s += string.Format("{0:N2}", sum) + "\n";
            }
            FilmViewing viewing = GetFilmViewing();
            s+= "\nStoelen:\n";
            foreach (int seatNumber in seats)
            {
                int[] seatXY = viewing.toRowandCol(seatNumber);
                double price = viewing.getTruePrice(seatNumber, GetFilm());
                s+= $"Rij: {seatXY[0]}, Stoel {seatXY[1]} ( {string.Format("{0:N2}", price)} )\n";
            }
            s += $"\nKaartjes: {string.Format("{0:N2}", seatPrice)}\n";
            s += "Totaalprijs: " + string.Format("{0:N2}", sum + seatPrice);
            return s;            
        }
        public void editConsumptions() {
            Tuple<double, List<string>, List<double>> addedSnacks = Consumptions.GetConsumptions();
            List<string> newSnacksText = new List<string>();
            newSnacksText.AddRange(this.snacks.Item2);
            newSnacksText.AddRange(addedSnacks.Item2);

            List<double> newSnacksPrice = new List<double>();
            newSnacksPrice.AddRange(this.snacks.Item3);
            newSnacksPrice.AddRange(addedSnacks.Item3);         

            Tuple<double, List<string>, List<double>> newSnacks = Tuple.Create(this.snacks.Item1 + addedSnacks.Item1, newSnacksText, newSnacksPrice);
            this.snacks = newSnacks;
            DataStorageHandler.SaveChanges();
        }

        public void removeConsumptions() {

            Tuple<double, List<string>, List<double>> newSnacks = Tuple.Create(0.0, new List<string>(), new List<double>());
            this.snacks = newSnacks;
            DataStorageHandler.SaveChanges();
            Console.Clear();
            Console.WriteLine($"Alle consumpties zijn verwijderd.\nKlik op enter om door te gaan");
            Console.ReadKey(false);
        }
        public void editSeats(FilmViewing viewing, List<int> seats) {

            int newSeat = FilmMenu.MakeReservation(viewing, seats);
            if (newSeat >= 0)
            {
                seats.Add(newSeat);
                double sum = 0.0; 
                foreach (Film film in DataStorageHandler.Storage.Films)
                    {
                        if (film.filmName == viewing.filmName) {
                            foreach (int seat in seats) {
                            sum += viewing.getTruePrice(seat, film);
                            }
                        }
                        seatPrice = sum;
                        // filmViewing.occupySeat(seat);
                    }
                DataStorageHandler.SaveChanges();
            }
            

        }
        public FilmViewing GetFilmViewing() {
            foreach (Film film in DataStorageHandler.Storage.Films) {
                if (film.filmName == this.filmName) {
                    foreach (FilmViewing viewing in film.viewingList)
                    {
                        if (viewing.viewingTime == this.filmViewingTime) {
                            return viewing;
                        }
                    }
                }
            }
            return null;
        }

        public Film GetFilm() {
            foreach (Film film in DataStorageHandler.Storage.Films) {
                if (film.filmName == this.filmName) {
                    return film;
                }
            }
            return null;
        }
    }
    
} 