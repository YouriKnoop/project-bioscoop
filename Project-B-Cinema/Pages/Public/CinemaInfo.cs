﻿using System;
using System.IO;
using System.Text.Json;

namespace Project_B_Cinema.Pages.Public
{
    class CinemaInfo
    {
        public static void GetCinema()
        {
            Console.Clear();
            string path;
            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
            if (pathing.Contains("bin/Debug")) {
                path = parent + "/databases/cinemaInfo.json";
            }
            else {
                path = pathing + "/databases/cinemaInfo.json";
            }
            var jsonData = File.ReadAllText(path);

            using JsonDocument doc = JsonDocument.Parse(jsonData);
            JsonElement root = doc.RootElement;
            Console.WriteLine("\nLocatiegegevens:");
            Console.WriteLine($"  Bioscoop naam: {root.GetProperty("name")}");
            Console.WriteLine($"  Adres: {root.GetProperty("city")}, {root.GetProperty("street")} {root.GetProperty("zip")}");
            Console.WriteLine($"\nOpeningstijden:");
            Console.WriteLine($"  ma/do: {root.GetProperty("midweek")}");
            Console.WriteLine($"  vr/zo: {root.GetProperty("weekend")}");
            Console.WriteLine($"\nContact:");
            Console.WriteLine($"  Telefoon: {root.GetProperty("phone")}");
            Console.WriteLine($"  E-mail: {root.GetProperty("email")}");
            Console.WriteLine($"\nDruk op enter om terug te gaan");

        }
    }
}
