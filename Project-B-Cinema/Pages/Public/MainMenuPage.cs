using System;
using System.Collections.Generic;
using System.Text;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using Project_B_Cinema.Admin;

namespace Project_B_Cinema.Pages.Public
{
    class MainMenuPage
    {

        public static User Run(User user)
        {
            DataStorageHandler.SaveChanges();
            Console.Clear();
            string Welcome = $"Welkom {user.UserName} bij de Bioscoop";
            string prompt = @"
  ______                    
 / ___(_)__  ___ __ _  ___ _
/ /__/ / _ \/ -_)  ' \/ _ \`/
\___/_/_//_/\__/_/_/_/\_,_/ 
                            
" + Welcome;

            // string prompt = $"Welkom {user.UserName} bij de Bioscoop";
            string[] options = { "Films bekijken", "Bioscoopinformatie bekijken", "Contact opnemen"};
            List<string> extraOptions = new List<string>(options);
            if (user.UserName == "Guest")
            {
                extraOptions.Add("Zoek Reservering");
            } else
            {
                extraOptions.Add("Mijn Reserveringen");
            }
            if (user.Role > 0) {
                prompt += " (Admin menu)";
                
                extraOptions.Add("Bioscoop beheer (admin)");
            }
            extraOptions.Add("Uitloggen");
            extraOptions.Add("Afsluiten");
            options = extraOptions.ToArray();
            ConsoleMenuHandler StartPagina = new ConsoleMenuHandler(prompt, options);
            StartPagina.DisplayOptions();
            int selectedIndex = StartPagina.Run();
/*
            if (options[selectedIndex] == "Login")
            {
                LoginPage.Login();
            }
            else if (options[selectedIndex] == "Registreren")
            {
                LoginPage.Registreren();
            }
*/
            switch (options[selectedIndex]) {
                case "Films bekijken":
                    FilmMenu.run(false, user);
                    break;
                case "Bioscoopinformatie bekijken":
                    CinemaInfo.GetCinema();
                    Console.ReadLine(); // Testline 
                    break;
                
                case "Mijn Reserveringen":
                    // TODO: Meer doen met 'mijn reserveringen'
                        myReservationsMenu.run(user);
                    break;

                case "Zoek Reservering":
                    // TODO: Meer doen met 'mijn reserveringen'
                        SearchReservation.run();
                    break;

                case "Contact opnemen":
                    Contact.ContactRun(user);
                    Console.ReadLine();
                    break;
                

                case "Bioscoopinformatie aanpassen":
                    Admin.CinemaInfoAdmin.GetCinema();
                    break;

                case "Bioscoop beheer (admin)":
                    AdminRun(user);
                    break;
                case "Uitloggen":
                    return null;
                case "Afsluiten":
                    //Console.WriteLine("Afsluiten - nog niet geimplementeerd");
                    //Console.ReadLine(); // Testline
                    System.Environment.Exit(0);
                    break;
                
                }
            return user;

        }
        public static void AdminRun(User user)
        {
            if (user.Role == 0){
                return;
            }

            bool adminRun = true;

            while(adminRun){
                DataStorageHandler.SaveChanges();
                Console.Clear();
                string prompt = $"Bioscoop beheren";
                string[] options = { "Bioscoop informatie aanpassen", "Films beheren", "Reserveringen beheren", "Zalen beheren", "Consumpties beheren", "Terug"};

                ConsoleMenuHandler StartPagina = new ConsoleMenuHandler(prompt, options);
                StartPagina.DisplayOptions();
                int selectedIndex = StartPagina.Run();

                switch (options[selectedIndex]) {
                    case "Bioscoop informatie aanpassen":
                        Admin.CinemaInfoAdmin.GetCinema();
                        break;
                    case "Films beheren":
                        FilmMenu.run(true, user);
                        break;

                    case "Reserveringen beheren":
                        AdminFilmsReservationsMenu.run();
                        break;

                    case "Zalen beheren":
                        HallMenu.Update();
                        HallMenu.run();
                        break;
                    
                    case "Consumpties beheren":
                        Admin.ConsumptionsAdmin.ConsumtionsAdminRun(user);
                        break;

                    case "Terug":
                        adminRun = false;
                        break;
                    
                    }
            }
        }

    }
}
