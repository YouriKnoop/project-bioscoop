using System;
using Project_B_Cinema.Datahandler;
using Project_B_Cinema.Classes;
using System.Collections.Generic;
using Project_B_Cinema.Pages;


namespace Project_B_Cinema {
    class AdminFilmsReservationsMenu {
        public static void run() {
            string prompt = "Selecteer een film";
            List<string> filmnaamlijst = new List<string>{};
            List<Film> filmlijst = new List<Film>{};

            foreach (Film film in DataStorageHandler.Storage.Films) {
                filmnaamlijst.Add(film.filmName);
                filmlijst.Add(film);
            }
            filmnaamlijst.Add("Terug");
            string[] options = filmnaamlijst.ToArray();
            ConsoleMenuHandler filmSelection = new ConsoleMenuHandler(prompt, options);
            filmSelection.DisplayOptions();
            int selectedIndex = filmSelection.Run();
            foreach (Film film in filmlijst) {
                if (filmnaamlijst[selectedIndex] == film.filmName) {
                    selectViewing(film);
                }
            }
            
        }

        public static void selectViewing(Film film) {
            string prompt = "Selecteer viewing";
            int i = 0;
            string[] options = new string[film.viewingList.Count + 1];
            film.viewingList.Sort((x, y) => x.viewingTime.CompareTo(y.viewingTime));
            foreach(FilmViewing viewing in film.viewingList) {
                options[i] = viewing.filmName + " @ " + viewing.viewingTime.ToUniversalTime();
                i++;
            }
            options[i] = "Terug";
            ConsoleMenuHandler viewingSelection = new ConsoleMenuHandler(prompt, options);
            viewingSelection.DisplayOptions();
            int selectedIndex = viewingSelection.Run();
            if (selectedIndex < film.viewingList.Count) {
                Reservation reservation  = selectReservation(film.viewingList[selectedIndex]);
                if (reservation != null) {
                    string s = reservation.printReservation();
                    bool result = myReservationsMenu.editOrBack(s);
                    while (result) {
                        string printedReservation = film.viewingList[selectedIndex].printHallWithReservationsToString(reservation.seats);
                        while (myReservationsMenu.editReservation(printedReservation, reservation, film.viewingList[selectedIndex])) {
                            printedReservation = film.viewingList[selectedIndex].printHallWithReservationsToString(reservation.seats);                                        
                        }
                        s = reservation.printReservation();
                        result = myReservationsMenu.editOrBack(s);
                    }
                    Console.Clear();
                    Console.WriteLine(reservation.printReservation());
                }
                else {
                    AdminFilmsReservationsMenu.run();
                }   
            }
            else {
                AdminFilmsReservationsMenu.run();
            }
            // Console.ReadLine();
        }

        public static Reservation selectReservation(FilmViewing viewing) {
            string prompt = "Selecteer reservering";
            int i = 0;
            Reservation[] reservations = viewing.GetReservations();
            string[] options = new string[reservations.Length + 1];
            foreach (Reservation reservation in reservations)
            {
                if (reservation.user.UserName != "Guest" && reservation.user.UserName != "Admin") {
                    options[i] = $"{reservation.id} Gebruikersnaam: {reservation.user.UserName} - {reservation.user.FirstName} {reservation.user.LastName}";
                    i++;
                }
                else {
                    options[i] = $"{reservation.id} {reservation.user.Email}";
                    i++;
                }
            }
            options[i] = "Terug";

            ConsoleMenuHandler selectReserveringen = new ConsoleMenuHandler(prompt, options);
            selectReserveringen.DisplayOptions();
            int selectedIndex = selectReserveringen.Run();
            if (selectedIndex < reservations.Length) {
                return reservations[selectedIndex];
            }
            return null;
        }
    }
}