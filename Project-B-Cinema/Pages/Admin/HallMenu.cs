﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Project_B_Cinema.Classes;

namespace Project_B_Cinema.Admin
{
    public class HallMenu
    {

        public static AllHalls AllHallsObject { get; set; }
        private static string path;

        public static void SaveChanges()
        {
            string result = JsonConvert.SerializeObject(AllHallsObject);
            File.WriteAllText(path, result);
        }

        public static void Update()
        {

            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
            if (pathing.Contains("bin/Debug")) {
                path = parent + "/databases/AllHalls.json";
            }
            else {
                
                path = pathing + "/databases/AllHalls.json";
            }

            if (!(File.Exists(path)))
            {
                using StreamWriter sw = File.CreateText(path);
                Console.WriteLine("File niet gevonden, nieuwe file aangemaakt.");
            }

            string filecontent = File.ReadAllText(path);

            try
            {
                AllHallsObject = JsonConvert.DeserializeObject<AllHalls>(filecontent);
                if (AllHallsObject == null)
                {
                    AllHallsObject = new AllHalls();
                }
            }
            catch (Exception)
            {
                AllHallsObject = new AllHalls();
            }
        }


        public HallMenu()
        {

            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;

            path = parent + "/databases/AllHalls.json";


            if (!(File.Exists(path)))
            {
                using StreamWriter sw = File.CreateText(path);
                Console.WriteLine("Niet gevonden");
            }

            string filecontent = File.ReadAllText(path);

            try
            {
                AllHallsObject = JsonConvert.DeserializeObject<AllHalls>(filecontent);
                if (AllHallsObject == null)
                {
                    AllHallsObject = new AllHalls();
                }
            }
            catch (Exception)
            {
                AllHallsObject = new AllHalls();
            }
        }

        public static void run()
        {
            bool showMenu = true;
            while (showMenu == true)
            {
                showMenu = showHallMenuFancy();
            }
        }

        public static bool showHallMenu()
        {
            Console.WriteLine("-------------------------------");
            Console.WriteLine("Zalen Menu");
            Console.WriteLine("[1] Alle zalen laten zien");
            Console.WriteLine("[2] Layout van een zaal inzien");
            Console.WriteLine("[3] Zaal toevoegen");
            Console.WriteLine("[4] Zaal verwijderen");
            Console.WriteLine("[5] Verwijder alle zalen");
            Console.WriteLine("[6] Reset naar de standaard zalen");
            Console.WriteLine("[7] Verander stoel prijzen");
            Console.WriteLine("[8] Terug");
            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    AllHallsObject.printAllHalls();
                    return true;

                case "2":
                    AllHallsObject.showHallLayout();
                    return true;
                case "3":
                    AllHallsObject.AddHall();
                    SaveChanges();
                    return true;
                case "4":
                    AllHallsObject.removeHall();
                    SaveChanges();
                    return true;
                case "5":
                    Console.WriteLine("Weet je zeker dat je alle zalen wil verwijderen?");
                    Console.WriteLine("[1] Ja");
                    Console.WriteLine("[2] Nee");
                    string c = Console.ReadLine();
                    if (c == "1")
                    {
                        AllHallsObject.removeAllHalls();
                        Console.WriteLine("Alle zalen zijn verwijderd!");
                        SaveChanges();
                    }
                    else
                    {
                        Console.WriteLine("Verwijderen van de zalen onderbroken.");
                    }
                    return true;
                case "6":
                    Console.WriteLine("Weet je zeker dat je de standaard zalen wil herstellen?");
                    Console.WriteLine("(Dit verwijdert alle zalen en vervangt ze door de standaard zaal150, zaal300 en zaal500.");
                    Console.WriteLine("[1] Ja");
                    Console.WriteLine("[2] Nee");
                    if (Console.ReadLine() == "1")
                    {
                        AllHallsObject.resetToDefaultHalls();
                        Console.WriteLine("Klaar! Alle standaard zalen zijn hersteld.");
                    }
                    else
                    {
                        Console.WriteLine("Herstellen van de standaard zalen is onderbroken.");
                    }
                    SaveChanges();
                    return true;
                case "7":
                    Console.WriteLine("Van welk stoeltype wil je de prijs veranderen?");
                    Console.WriteLine("[1] Regular seats ( . ): " + AllHallsObject.seatPrices[0].ToString("C"));
                    Console.WriteLine("[2] Viewer seats ( + ): " + AllHallsObject.seatPrices[1].ToString("C"));
                    Console.WriteLine("[3] Premium seats ( * ): " + AllHallsObject.seatPrices[2].ToString("C"));
                    Console.WriteLine("[4] Terug");
                    int toChange = Int32.Parse(Console.ReadLine()) - 1;
                    if (toChange != 3)
                    {
                        Console.WriteLine("\rNieuwe Prijs: ");
                        double newPrice = Double.Parse(Console.ReadLine());
                        AllHallsObject.changeSeatPrices(toChange, newPrice);
                        Console.WriteLine("Stoel prijs is veranderd.");
                        SaveChanges();
                    }
                    else
                    {
                        Console.WriteLine("Veranderen van stoel prijs is onderbroken.");
                    }

                    return true;
                case "8":
                    return false;
                default:
                    return true;
            }
        }


        public static int fancyMenu(string[] options, string before, string after)
        {

            ConsoleKey keyPressed = ConsoleKey.Z;
            int selectedIndex = 0;
            while (keyPressed != ConsoleKey.Spacebar && keyPressed != ConsoleKey.Enter)

            {



                Console.Clear();

                if (before != null)
                {
                    Console.WriteLine(before);
                }
                
                for (int i = 0; i < options.Length; i++)
                {
                    if (selectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    else
                    {
                        Console.ResetColor();
                    }
                    Console.WriteLine(options[i]);
                    Console.ResetColor();
                }

                Console.ResetColor();

                if (after != null)
                {
                    Console.WriteLine(after);
                    Console.ResetColor();
                }
                

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                keyPressed = keyInfo.Key;


                if (keyPressed == ConsoleKey.UpArrow)
                {
                    selectedIndex--;
                }
                else if (keyPressed == ConsoleKey.DownArrow)
                {
                    selectedIndex++;
                }

                if (selectedIndex == -1)
                {
                    selectedIndex = options.Length - 1;
                }
                else if (selectedIndex == options.Length)
                {
                    selectedIndex = 0;
                }
            }
            Console.Clear();
            selectedIndex += 1;
            return selectedIndex;
        }




        public static bool showHallMenuFancy()
        {
            int selectedIndex = 0;
            int c = 0;
            string[] lines = new string[8] { "[1] Alle zalen laten zien",
                                             "[2] Layout van een zaal inzien",
                                             "[3] Zaal toevoegen",
                                             "[4] Zaal verwijderen",
                                             "[5] Verwijder alle zalen",
                                             "[6] Reset naar de standaard zalen",
                                             "[7] Verander stoel prijzen",
                                             "[8] Terug"};
            selectedIndex = fancyMenu(lines, "-------------------------------\nZalen Menu", "-------------------------------");


            switch (selectedIndex)
            {
                case 1:
                    AllHallsObject.printAllHalls();
                    Console.WriteLine($"Klik op enter om terug te gaan");
                    Console.ReadKey(true);
                    return true;

                case 2:
                    if (AllHallsObject.hallListCheck())
                    {
                        List<string> options = new List<string>();
                        int i = 1;
                        if(AllHallsObject.theHalls.Count < 1){
                            Console.WriteLine("Er zijn geen zalen om de layout van te bekijken!");
                            Console.WriteLine($"Klik op enter om terug te gaan");
                            Console.ReadKey(true);
                            return true;
                        }
                        foreach (Hall currHall in AllHallsObject.theHalls)
                        {
                            options.Add($"[{i}]: {currHall.name()}");
                            i++;
                        }

                        options.Add($"[{i++}]: terug");

                        c = fancyMenu(options.ToArray(), "Selecteer een zaal om de layout van te bekijken: ", null);
                        c--;
                        if(AllHallsObject.theHalls.Count == c){
                            return true;
                        }
                        Console.Clear();
                        Console.WriteLine("-------------------------------");
                        Console.WriteLine(AllHallsObject.theHalls[c].hallName);
                        Console.WriteLine("-------------------------------");
                        AllHallsObject.theHalls[c].printHall();
                        Console.WriteLine("[1] Regular seats ( . ): " + AllHallsObject.seatPrices[0].ToString("C"));
                        Console.WriteLine("[2] Viewer seats ( + ): " + AllHallsObject.seatPrices[1].ToString("C"));
                        Console.WriteLine("[3] Premium seats ( * ): " + AllHallsObject.seatPrices[2].ToString("C"));
                    }
                        else
                        {
                        Console.WriteLine("Er zijn geen zalen om de layout van te bekijken!");
                    }
                    Console.WriteLine($"Klik op enter om terug te gaan");
                    Console.ReadKey(true);
                    return true;
                case 3:
                    AllHallsObject.AddHall();
                    Console.WriteLine($"Klik op enter om door te gaan");
                    Console.ReadKey(true);
                    SaveChanges();
                    return true;
                case 4:
                    if (AllHallsObject.hallListCheck())
                    {
                        int i = 1;
                        List<string> options = new List<string>();
                        foreach (Hall currHall in AllHallsObject.theHalls)
                        {
                            options.Add($"[{i}]: {currHall.name()}");
                            i++;
                        }
                        options.Add($"[{i}]: Terug");
                        int removeThis = fancyMenu(options.ToArray(), "Selecteer een zaal om te verwijderen: ", null);
                        removeThis--;
                        if (removeThis < options.Count - 1)
                        {
                            AllHallsObject.theHalls.RemoveAt(removeThis);
                            Console.Clear();
                            Console.WriteLine($"Klaar! Zaal verwijderd op index {removeThis + 1}");
                            Console.WriteLine($"Klik op enter om door te gaan");
                            Console.ReadKey(true);
                            SaveChanges();
                        }
                        
                    }
                    else
                    {
                        Console.WriteLine("Er zijn geen zalen! Voeg alstublieft zalen toe met 'Voeg zalen toe'.");
                    }
                    
                    return true;
                case 5:

                    c = fancyMenu(new string[] { "Ja", "Nee" }, "Weet je zeker dat je alle zalen wil verwijderen?", null);
                    if (c == 1)
                    {
                        AllHallsObject.removeAllHalls();
                        Console.WriteLine("Alle zalen zijn verwijderd!");
                        Console.WriteLine($"Klik op enter om door te gaan");
                        Console.ReadKey(true);
                        SaveChanges();
                    }
                    else
                    {
                        Console.WriteLine("Verwijderen van de zalen onderbroken.");
                        Console.WriteLine($"Klik op enter om terug te gaan");
                        Console.ReadKey(true);
                    }
                    return true;
                case 6:
                    c = fancyMenu(new string[] { "Ja", "Nee" }, "Weet je zeker dat je de standaard zalen wil herstellen?\n(Dit verwijdert alle zalen en vervangt ze door de standaard zaal150, zaal300 en zaal500.", null);

                    if (c == 1)
                    {
                        AllHallsObject.resetToDefaultHalls();
                        SaveChanges();
                        Console.WriteLine("Klaar! Alle standaard zalen zijn hersteld.");
                        Console.WriteLine($"Klik op enter om door te gaan");
                        Console.ReadKey(true);
                        
                    }
                    else
                    {
                        Console.WriteLine("Herstellen van de standaard zalen is onderbroken.");
                        Console.WriteLine($"Klik op enter om terug te gaan");
                        Console.ReadKey(true);
                    }
                    return true;
                case 7:
                    string[] seatPriceStrings = new string[] { $"[1] Regular seats ( . ): " + AllHallsObject.seatPrices[0].ToString("C"), $"[2] Viewer seats ( + ): " + AllHallsObject.seatPrices[1].ToString("C"), "[3] Premium seats ( * ): " + AllHallsObject.seatPrices[2].ToString("C"), "[4] Terug" };
                    c = fancyMenu(seatPriceStrings, "Van welk stoeltype wil je de prijs veranderen?", null);
                    c--;
                    if (c != 3)
                    {
                        Console.WriteLine("\rNieuwe Prijs: ");
                        string newPriceString = Console.ReadLine();
                        double newPrice;
                        while (double.TryParse(newPriceString, out newPrice) == false)
                        {
                            Console.WriteLine("Vul alstublieft een nummer in!");
                            Console.WriteLine("Nieuwe Prijs: ");
                            newPriceString = Console.ReadLine();
                            newPriceString = newPriceString.Trim(new char[] { '$', '€' });
                        }
                        AllHallsObject.changeSeatPrices(c, newPrice);
                        Console.Clear();
                        SaveChanges();
                        Console.WriteLine("Stoel prijs is veranderd.");
                        Console.WriteLine($"Klik op enter om door te gaan");
                        Console.ReadKey(true);
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Veranderen van stoel prijs is onderbroken.");
                        Console.WriteLine($"Klik op enter om terug te gaan");
                        Console.ReadKey(true);
                    }

                    return true;
                case 8:
                    return false;
                default:
                    return true;

            }
        }
    }
}
