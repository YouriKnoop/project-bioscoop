using System;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;

namespace Project_B_Cinema.Pages.Admin
{
    class CinemaInfoAdmin
    {
        public string name { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string midweek { get; set; }
        public string weekend { get; set; }


        public static void GetCinema()
        {
            CinemaInfoAdmin adminInfo = loadCinemaInfo();
            
            bool Bool = true;

            while(Bool){

                Console.Clear();
                string path;
                string pathing = Environment.CurrentDirectory;
                string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
                if (pathing.Contains("bin/Debug"))
                {
                    path = parent + "/databases/cinemaInfo.json";
                }
                else
                {
                    path = pathing + "/databases/cinemaInfo.json";
                }
                var jsonData = File.ReadAllText(path);

                using JsonDocument doc = JsonDocument.Parse(jsonData);
                JsonElement root = doc.RootElement;

                Console.WriteLine($"Bioscoopinfo aanpassen\n");
                Console.WriteLine($"[1] Bioscoop naam: {root.GetProperty("name")}");
                Console.WriteLine($"[2] Telefoon: {root.GetProperty("phone")}");
                Console.WriteLine($"[3] E-mail: {root.GetProperty("email")}");

                Console.WriteLine($"\nAdres:");
                Console.WriteLine($"[4] Stad: {root.GetProperty("city")}");
                Console.WriteLine($"[5] Straat + huisnummer: {root.GetProperty("street")}");
                Console.WriteLine($"[6] Postcode: {root.GetProperty("zip")}");

                Console.WriteLine($"\nOpeningstijden:");
                Console.WriteLine($"[7] Doordeweeks: {root.GetProperty("midweek")}");
                Console.WriteLine($"[8] Weekend: {root.GetProperty("weekend")}");

                Console.WriteLine($"\nVul het nummer in en klik op enter");
                Console.WriteLine($"Of klik op enter om terug te gaan");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.WriteLine("Je koos voor [1] Bioscoop naam");
                        Console.WriteLine("Naar welke naam wil je het veranderen?");
                        adminInfo.name = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "2":
                        Console.WriteLine("Je koos voor [2] Telefoonnummer");
                        Console.WriteLine("Naar welk nummer wil je het veranderen?");
                        adminInfo.phone = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "3":
                        Console.WriteLine("Je koos voor [3] email adres");
                        Console.WriteLine("Naar welk adres wil je het veranderen?");
                        adminInfo.email = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "4":
                        Console.WriteLine("Je koos voor [4] stad");
                        Console.WriteLine("Naar welke stad wil je het veranderen?");
                        adminInfo.city = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "5":
                        Console.WriteLine("Je koos voor [5] straat + huisnummer");
                        Console.WriteLine("Naar welk adres wil je het veranderen?");
                        adminInfo.street = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "6":
                        Console.WriteLine("Je koos voor [6] postcode");
                        Console.WriteLine("Naar welke postcode wil je het veranderen?");
                        adminInfo.zip = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "7":
                        Console.WriteLine("Je koos voor [7] openingstijden doordeweeks");
                        Console.WriteLine("Naar welke naam wil je het veranderen?");
                        adminInfo.midweek = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "8":
                        Console.WriteLine("Je koos voor [8] openingstijden weekend");
                        Console.WriteLine("Naar welke naam wil je het veranderen?");
                        adminInfo.weekend = Console.ReadLine();
                        changeCinemaInfo(adminInfo);
                        break;

                    case "":
                        Bool = false;
                        break;

                    default:
                        Console.WriteLine("default");
                        break;

                }
            }
        }

        public static CinemaInfoAdmin loadCinemaInfo()
        {
            string path = "";
            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
            if (pathing.Contains("bin/Debug"))
            {
                path = parent + "/databases/cinemaInfo.json";

            }
            else
            {
                path = pathing + "/databases/cinemaInfo.json";
            }
            using (StreamReader r = new StreamReader(path))
            {
                return JsonConvert.DeserializeObject<CinemaInfoAdmin>(r.ReadToEnd());
            }
        }

        public static void changeCinemaInfo(CinemaInfoAdmin data)
        {
            string path = "";
            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
            if (pathing.Contains("bin/Debug"))
            {
                path = parent + "/databases/cinemaInfo.json";

            }
            else
            {
                path = pathing + "/databases/cinemaInfo.json";
            }
            File.WriteAllText(path, System.Text.Json.JsonSerializer.Serialize(data));
        }
    }
}
