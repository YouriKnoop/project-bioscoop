using System;
using System.Collections.Generic;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using System.Text.RegularExpressions;

namespace Project_B_Cinema
{
    public class SearchReservation
    {
        public static void run() {
            Console.Clear();
            List<Reservation> possibleReservations = new List<Reservation>{};

            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            string UserEmail = "";
            int reservationId = -9999;
            bool inValidEmail = true;
            while (inValidEmail)
            {
                Console.WriteLine("Email Adres: ");
                string emailValidate = Console.ReadLine();


                // verfify if it is a valid email with a @
                if (emailRegex.IsMatch(emailValidate))
                {
                    UserEmail = emailValidate;

                    // end loop
                    inValidEmail = false;
                }
                else
                {
                    Console.WriteLine("Email onjuist, probeer opnieuw");
                }
            }

            while (reservationId == -9999) {
                Console.WriteLine("Reservering code:");
                string id = Console.ReadLine();
                try {
                    reservationId = Int32.Parse(id);
                }
                catch {
                    Console.WriteLine("Input niet goed, voer aub een geheel getal in.");
                }
            }

            foreach (Reservation reservation in DataStorageHandler.Storage.Reservations) {
                if (reservation.user.Email == UserEmail && reservation.id == reservationId) {
                    possibleReservations.Add(reservation);
                }
            };

            if(possibleReservations.Count == 0){
                editOrBack();
            }else if(possibleReservations.Count == 1){
                Reservation myReservation = possibleReservations[0];
                string s = myReservation.printReservation();
                bool result = myReservationsMenu.editOrBack(s);
                while (result) {
                    foreach (Film film in DataStorageHandler.Storage.Films) {
                        if (myReservation.filmName == film.filmName) {
                            foreach (FilmViewing viewing in film.viewingList)
                            {
                                if (myReservation.filmViewingTime == viewing.viewingTime) {
                                    //Console.WriteLine(viewing.printHallWithReservationsToString(myReservation.seats));
                                    string printedReservation = viewing.printHallWithReservationsToString(myReservation.seats);
                                    while (myReservationsMenu.editReservation(printedReservation, myReservation, viewing)) {
                                        printedReservation = viewing.printHallWithReservationsToString(myReservation.seats);                                        
                                    }
                                }
                            }
                        }
                    }
                    s = myReservation.printReservation();
                    result = myReservationsMenu.editOrBack(s);
                    
                }
                Console.Clear();
                Console.WriteLine(myReservation.printReservation());
            }

            // return possibleReservations

        }

        public static void editOrBack() {
            ConsoleMenuHandler editOrBack = new ConsoleMenuHandler("Reservering onjuist", new string[]{"Opnieuw zoeken", "Terug"});
            editOrBack.DisplayOptions();
            int selectedIndex = editOrBack.Run();
            if (selectedIndex == 0) {
                run();
            }
            else {
                return;
            }
        }
    }
}