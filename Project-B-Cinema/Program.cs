﻿using System;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using System.ComponentModel;
using Project_B_Cinema.Pages.Public;
using Project_B_Cinema.Pages;
using System.Globalization;
using System.IO;

namespace Project_B_Cinema
{
    class Program
    {
        private static void Main(string[] args)
        {
            CultureInfo.CurrentCulture = new CultureInfo("NL-nl");
            string path = "";
            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
            if (pathing.Contains("bin/Debug"))
            {
                path = parent + "/databases/ProjectB.json";
            }
            else
            {

                path = pathing + "/databases/ProjectB.json";
            }
            DataStorageHandler.Init(path);
            User user = WelcomePage.Run();
            while (true)
            {
                
                while (user == null)
                {
                    user = WelcomePage.Run();
                }
                while (user != null)
                {
                    user = MainMenuPage.Run(user);
                }
            }
        }

    }
}
