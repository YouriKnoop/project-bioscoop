using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Admin;

namespace Project_B_Cinema
{
    class Menus
    {
        public static bool ShowMainMenu()
        {
            //Console.Clear();
            Console.WriteLine("Welkom bij mijn super deluxe JSON test Console App \n");

            Console.WriteLine("Option 1: Add a User\n");
            Console.WriteLine("Option 2: See User Database\n");
            Console.WriteLine("Option 3: Halls\n");
            Console.WriteLine("Option 4: Close menu/App\n");
            

            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    AddUserMenu();
                    return true;
                case "2":
                    Console.WriteLine("\nGoed gedaan pik je koos voor 2");
                    string pathing = Environment.CurrentDirectory;
                    string parent = Directory.GetParent(pathing).Parent.Parent.FullName;

                    string path = parent + "/databases/users.json";
                    Console.WriteLine(File.ReadAllText(path));
                    return true;
                case "3":
                    goToHallMenu();
                    return true;
                case "4":
                    return false;
                
                default:
                    return true;

            }
        }

        private static void AddUserMenu()
        {
            Console.Clear();

            Console.WriteLine("Option 1: You want to add a User\n");

            Console.Write("\r\n Please give a username: \n");
            String userName = Console.ReadLine();

            Console.Write("\r\nPlease give a email: \n");
            String userEmail = Console.ReadLine();

            Console.WriteLine($"Hello, {userName}! Je gegeven email is {userEmail}");

            User user1 = new User { UserName = userName, Email = userEmail };

            var jsonString = JsonConvert.SerializeObject(user1);

            Console.Write(jsonString + "\n");

            dynamic json = JsonConvert.DeserializeObject(jsonString);


            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;

            string path = parent + "/databases/users.json";

            File.AppendAllText(path, jsonString);

        }

        public static void goToHallMenu()
        {
            //Hall deze line hieronder weg als we een startup class/functie hebben.

            HallMenu.run();
        }

    }
}
