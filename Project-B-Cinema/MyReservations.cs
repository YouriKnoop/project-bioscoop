using System;
using System.Collections.Generic;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;

namespace Project_B_Cinema {
    public class myReservationsMenu {
        public static void run(User user) {
            List<Reservation> myReservations = new List<Reservation>{};
            foreach (Reservation reservation in DataStorageHandler.Storage.Reservations) {
                if (reservation.user.UserName == user.UserName) {
                    myReservations.Add(reservation);
                }
            };

            string prompt = "Mijn reserveringen:";
            string[] options = new string[myReservations.Count + 1];
            int i = 0;
            foreach (Reservation reservation in myReservations) {
                options[i] = reservation.filmName + " "+ reservation.filmViewingTime.ToUniversalTime();
                i++;
            }
            options[i] = "Terug";
            ConsoleMenuHandler myReservationsPagina = new ConsoleMenuHandler(prompt, options);
            myReservationsPagina.DisplayOptions();
            int selectedIndex = myReservationsPagina.Run();
            

            if (selectedIndex < myReservations.Count) {
                Reservation myReservation = myReservations[selectedIndex];
                string s = myReservation.printReservation();
                bool result = editOrBack(s);
                while (result) {
                    foreach (Film film in DataStorageHandler.Storage.Films) {
                        if (myReservation.filmName == film.filmName) {
                            foreach (FilmViewing viewing in film.viewingList)
                            {
                                if (myReservation.filmViewingTime == viewing.viewingTime) {
                                    //Console.WriteLine(viewing.printHallWithReservationsToString(myReservation.seats));
                                    string printedReservation = viewing.printHallWithReservationsToString(myReservation.seats);
                                    while (editReservation(printedReservation, myReservation, viewing)) {
                                        printedReservation = viewing.printHallWithReservationsToString(myReservation.seats);

                                    }
                                }
                            }
                        }
                    }
                    s = myReservation.printReservation();
                    result = editOrBack(s);
                    
                }
                Console.Clear();
                Console.WriteLine(myReservation.printReservation());
                //Console.ReadLine();
                
            }
        }

        public static bool editOrBack(string reservationShortcode) {
            ConsoleMenuHandler editOrBack = new ConsoleMenuHandler(reservationShortcode, new string[]{"Bewerken", "Terug"});
            editOrBack.DisplayOptions();
            int selectedIndex = editOrBack.Run();
            if (selectedIndex == 0) {
                return true;
            }
            else {
                return false;
            }
        }
        public static bool editReservation(string reservationString, Reservation reservation, FilmViewing viewing) {
            Console.Clear();
            string[] options = {"Alle stoelen wissen", "Stoel toevoegen", "Consumpties aanpassen", "Alle consumpties verwijderen", "Terug"};
            ConsoleMenuHandler DeleteAddOrRemove = new ConsoleMenuHandler(reservationString, options);
            DeleteAddOrRemove.DisplayOptions();
            int deleteAddRemoveIndex = DeleteAddOrRemove.Run();
            switch (deleteAddRemoveIndex) {
                case 0:
                reservation.seats = new List<int>();
                reservation.editSeats(viewing, reservation.seats);
                DataStorageHandler.SaveChanges();
                return true;
                case 1:
                reservation.editSeats(viewing, reservation.seats);
                DataStorageHandler.SaveChanges();
                return true;
                case 2:
                reservation.editConsumptions();
                DataStorageHandler.SaveChanges();
                return false;          
                case 3:
                reservation.removeConsumptions();
                DataStorageHandler.SaveChanges();
                return false;
                case 4:
                return false;
            }
            return false;
        }
    }
}
