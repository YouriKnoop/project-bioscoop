﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Project_B_Cinema.Classes;

namespace Project_B_Cinema.Datahandler
{
    class DataStorage
    {
        public List<User> Users { get; set; } = new List<User>();
        public List<Reservation> Reservations { get; set; } = new List<Reservation>();
        public List<Consumptions> Consumptions { get; set; } = new List<Consumptions>();
        public List<Film> Films { get; set; } = new List<Film>();

    }
    
}
