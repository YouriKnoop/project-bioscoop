﻿using System;
using System.Collections.Generic;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using System.Globalization;

namespace Project_B_Cinema
{
    public class AllFilms
    {
        public List<Film> theFilms { get; set; }

        public AllFilms()
        {
            this.theFilms = Update();
        }

        public void addFilm(string name, string filmDescription, string actors, string genre, int age, double price, int filmduur)
        {
            Film newFilm = new Film(name, filmDescription, actors, genre, age, price, filmduur);
            DataStorageHandler.Storage.Films.Add(newFilm);
            this.theFilms = Update();
        }

        public List<Film> Update()
        {
            List<Film> allFilms = DataStorageHandler.Storage.Films;


            foreach(Film film in allFilms)
            {
                film.viewingList.Sort(CompareViewings);
            }
            DataStorageHandler.SaveChanges();
            return allFilms;
        }
        public static int CompareViewings(FilmViewing x, FilmViewing y)
        {
            if (x.viewingTime < DateTime.Now)
            {
                if (y.viewingTime < DateTime.Now)
                {
                    return DateTime.Compare(x.viewingTime, y.viewingTime);
                } else
                {
                    return 1;
                }
            } else if (y.viewingTime < DateTime.Now)
            {
                return -1;
            } else
            {
                return DateTime.Compare(x.viewingTime, y.viewingTime);
            }
        }
    }
}
