﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using System.Globalization;

namespace Project_B_Cinema.Pages.Public
{
    class LoginPage
    {
        public static User Login()
        {
            Console.Clear();
            List<User> usersList = DataStorageHandler.Storage.Users;

            string UserInput    = "";

            string Password     = "";

            while (true)
            {
                Console.Clear();
                UserInput = Beheer.Input("Voer uw e-mailadres of gebruikersnaam in: ");
                Password = Beheer.Input("Voer uw wachtwoord in: ");

                foreach (User currentUser in usersList)
                {
                    if ((UserInput == currentUser.Email || UserInput == currentUser.UserName) && Password == currentUser.Password)
                    {

                        return currentUser;
                    }
                }
                string prompt = "Informatie komt niet voor in ons systeem.";
                string[] options2 = { "Probeer opnieuw", "Terug" };
                ConsoleMenuHandler FailedLogin = new ConsoleMenuHandler(prompt, options2);
                FailedLogin.DisplayOptions();
                int selectedIndex = FailedLogin.Run();
                if (selectedIndex == 1)
                {
                    return null;
                }
            }

        }

        public static User Registreren()
        {
            Console.Clear();

            Regex userRegex = new Regex("^[ A-Za-z0-9]+$");
            Regex nameRegex = new Regex("^[ A-Za-z]+$");
            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Regex passwordRegex = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*()_+=\[{\]};:<>|./?,-]).{8,}$");

            string gebruikersNaam = null;
            string firstName = null;
            string lastName = null;
            string email = null;
            string password = null;
            DateTime birthday = new DateTime();


            bool inValidUserName = true;
            while (inValidUserName)
            {
                string gebruikersNaamValidate = Beheer.Input("Kies een Gebruikersnaam: ");

                if (userRegex.IsMatch(gebruikersNaamValidate))
                {
                    gebruikersNaam = gebruikersNaamValidate;

                    // end the loop
                    inValidUserName = false;
                }
                else
                {
                    Console.WriteLine("GebruikersNaam is Invalid!!! \n--Probeer opnieuw AUB");
                }
            }

            bool inValidPassword = true;
            while (inValidPassword)
            {
                string passwordValidate = "";
                bool validPassRegex = false;

                while(!validPassRegex){
                    passwordValidate = Beheer.Input("Kies een Wachtwoord: ");
                    // verfify if it is a valid email with a @
                    if (passwordRegex.IsMatch(passwordValidate))
                    {

                        validPassRegex = true;
                    }
                    else
                    {
                        Console.WriteLine("Maak gebruik van een hoofdletter, kleine letter, een speciaal teken, ten minste 1 getal en minimaal 8 tekens! \n--Probeer opnieuw AUB");
                    }
                }

                string password2Validate = Beheer.Input("Herhaal het Wachtwoord: ");

                if (passwordValidate == password2Validate)
                {
                    password = passwordValidate;

                    // end the loop
                    inValidPassword = false;
                }
                else
                {
                    Console.WriteLine("Wachtwoorden komen niet overeen!!! \n--Probeer opnieuw AUB");
                }
            }

            bool inValidEmail = true;
            while (inValidEmail)
            {
                string emailValidate = Beheer.Input("Wat is uw Emailadres?: ");


                // verfify if it is a valid email with a @
                if (emailRegex.IsMatch(emailValidate))
                {
                    email = emailValidate;

                    // end loop
                    inValidEmail = false;
                }
                else
                {
                    Console.WriteLine("Gebruikt een geldig emailadress!!! \n--Probeer opnieuw AUB");
                }

            }

            bool inValidBirthday = true;
            while (inValidBirthday)
            {
                try
                {
                    string birthdayInput = Beheer.Input("Wat is uw geboorte datum? [dd-mm-jjjj]: ");
                    birthday = DateTime.Parse(birthdayInput);
                    inValidBirthday = false;
                }
                catch (System.Exception)
                {
                    Console.WriteLine("Gebruik een geldige datum!");
                    
                }
            }

            bool inValidFirstname = true;
            while (inValidFirstname)
            {
                string firstNameValidate = Beheer.Input("Wat is uw voornaam?: ");

                // verify the firstname to contain only letters
                if (nameRegex.IsMatch(firstNameValidate))
                {
                    firstName = firstNameValidate;

                    // end loop
                    inValidFirstname = false;
                }
                else
                {
                    Console.WriteLine("Naam mag alleen uit letters bestaan!!! \n--Probeer opnieuw AUB");
                }
            }

            bool inValidLastname = true;
            while (inValidLastname)
            {
                string lastNameValidate = Beheer.Input("Wat is uw Achternaam?: ");

                // verify the lastname to contain only letters
                if (nameRegex.IsMatch(lastNameValidate))
                {
                    lastName = lastNameValidate;

                    // end loop
                    inValidLastname = false;
                }
                else
                {
                    Console.WriteLine("Achternaam mag alleen uit letters bestaan!!! \n--Probeer opnieuw AUB");
                }
            }

            User User = new User
            {
                UserID = Guid.NewGuid().ToString(),
                UserName = gebruikersNaam,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Password = password,
                Birthday = birthday
            };

            DataStorageHandler.Storage.Users.Add(User);
            //WelcomePage.Run();

            return User;
        }
    }
}
