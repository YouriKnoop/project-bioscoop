﻿using System;
using System.Collections.Generic;
using System.Text;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;

namespace Project_B_Cinema.Pages.Public
{
    class WelcomePage
    {

        public static User Run()
        {

            DataStorageHandler.SaveChanges();
            Console.Clear();
            string prompt = @"
  ______                    
 / ___(_)__  ___ __ _  ___ _
/ /__/ / _ \/ -_)  ' \/ _ \`/
\___/_/_//_/\__/_/_/_/\_,_/ 
                            
";
            // string[] options = { "Login", "Registreren" , "Doorgaan als gast", "Doorgaan als admin", "Afsluiten"};
            string[] options = { "Login", "Registreren" , "Doorgaan als gast", "Afsluiten"};

            ConsoleMenuHandler StartPagina = new ConsoleMenuHandler(prompt, options);
            StartPagina.DisplayOptions();
            int selectedIndex = StartPagina.Run();
/*
            if (options[selectedIndex] == "Login")
            {
                LoginPage.Login();
            }
            else if (options[selectedIndex] == "Registreren")
            {
                LoginPage.Registreren();
            }
*/
            switch (options[selectedIndex]) {
                case "Login":
                    return LoginPage.Login();
                case "Registreren":
                    return LoginPage.Registreren();
                    
                case "Doorgaan als gast":
                    Console.WriteLine("Ga door als gast");
                    return new User{
                        UserName = "Guest",
                    };
                /*
                case "Doorgaan als admin":
                    Console.WriteLine("Ga door als admin");
                    return new User{
                        UserName = "Admin",
                        Role = 1,
                    };
                */
                case "Afsluiten":
                    System.Environment.Exit(0);
                    break;
            }    
            return Run();       

        }


    }
}
