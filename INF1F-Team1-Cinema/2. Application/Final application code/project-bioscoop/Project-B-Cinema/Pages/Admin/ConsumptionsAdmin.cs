using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Text.Json;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using Project_B_Cinema.Admin;

namespace Project_B_Cinema.Pages.Admin
{
    public class ConsumptionsAdmin
    {
        public int ID { get; set; }

        public string Type { get; set; }

        public string Sort { get; set; }

        public double Price { get; set; }
        public static void ConsumtionsAdminRun(User user)
        {
            if (user.Role == 0){
                return;
            }

            DataStorageHandler.SaveChanges();
            bool Bool = true;

            while(Bool){
                Console.Clear();
                string prompt = $"Consumpties beheren";
                string[] options = {"Consumpties bekijken", "Consumpties toevoegen", "Consumpties verwijderen", "Terug"};

                ConsoleMenuHandler StartPagina = new ConsoleMenuHandler(prompt, options);
                StartPagina.DisplayOptions();
                int selectedIndex = StartPagina.Run();


                switch (options[selectedIndex]) {
                    case "Consumpties bekijken":
                        displayConsumptions();
                        Console.WriteLine("Klik enter om terug te gaan");
                        Console.ReadLine();
                        break;
                    case "Consumpties toevoegen":
                        AddConsumption();
                        break;
                    case "Consumpties verwijderen":
                        displayConsumptions();
                        removeConsumptions();
                        break;

                    case "Terug":
                        Bool = false;
                        break;
                    
                }
            }

        }

        public static void AddConsumption(){
            Console.Clear();
            Console.WriteLine("Consumpties toevoegen");
            List<Consumptions> consumptionList = DataStorageHandler.Storage.Consumptions;
            int lastID = consumptionList.Last().ID;
            int id = lastID + 1;
            Console.WriteLine("Type(eten):");
            string type = Console.ReadLine();
            Console.WriteLine("Soort(cola):");
            string sort = Console.ReadLine();


            Console.WriteLine("\rPrijs (gebruik komma als decimaal teken): ");
            string priceString = Console.ReadLine();
            double price;
            while (double.TryParse(priceString, out price) == false)
            {
                Console.WriteLine("Vul alstublieft een nummer in!");
                Console.WriteLine("\rPrijs: ");
                priceString = Console.ReadLine();
            }

            Consumptions Consumption = new Consumptions
            {
                ID = id,
                Type = type,
                Sort = sort,
                Price = price
            };

            DataStorageHandler.Storage.Consumptions.Add(Consumption);
        }

        public static void displayConsumptions(){
            Console.Clear();
            List<Consumptions> consumptionList = DataStorageHandler.Storage.Consumptions;

            // move through List
            for(int i = 0; i < consumptionList.Count; i++)
            {
                Console.WriteLine($"[{consumptionList[i].ID}] {consumptionList[i].Sort} {consumptionList[i].Price.ToString("C")}");
            }
            
        }

        public static void removeConsumptions(){
            List<Consumptions> consumptionList = DataStorageHandler.Storage.Consumptions;
            Console.WriteLine("---------------------------");
            Console.WriteLine("Welk nummer wilt u verwijderen? (Klik op enter om terug te gaan): ");
            string remove = Console.ReadLine();
            if(remove.ToLower() == ""){
                return;
            }

            
            int removeInt;
            while (int.TryParse(remove, out removeInt) == false)
            {
                Console.WriteLine("Vul alstublieft een nummer in!");
                Console.WriteLine("Welk nummer wilt u verwijderen? (Klik op enter om terug te gaan): ");
                remove = Console.ReadLine();
                if(remove.ToLower() == ""){
                    return;
                }
            }
            try
            {
                // consumptionList.RemoveAt(removeInt - 1);
                Console.Clear();
                Console.WriteLine($"Item {consumptionList.Single(x => x.ID == removeInt).ID}, {consumptionList.Single(x => x.ID == removeInt).Sort} is verwijderd!");
                consumptionList.Remove(consumptionList.Single(x => x.ID == removeInt));
                Console.WriteLine($"Klik op enter om door te gaan");
                Console.ReadLine();
            }
            catch (System.Exception)
            {
                Console.WriteLine("Verwijderen mislukt! Consumptie bestaat niet!");
                Console.WriteLine($"Klik op enter om terug te gaan");
                Console.ReadLine();
            }
            
            
        }
        
    }
}