using System;
using System.Threading;
using System.Net;
using System.Net.Mail;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Pages.Admin;
using System.Text.RegularExpressions;

namespace Project_B_Cinema
{
    public class Contact
    {
        public static void ContactRun(User currentUser)
        {
            Console.Clear();
            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            CinemaInfoAdmin adminInfo = CinemaInfoAdmin.loadCinemaInfo();
            string CinemaMail = adminInfo.email;
            string Host = "@gmail.com";
            string UserEmail = "";

            if (!CinemaMail.Contains(Host))
            {
                Console.WriteLine("De email van de bioscoop is fout. \nProbeer handmatig contact op te nemen via de aangegeven informatie in het 'bioscoop informatie' kopje");
                return;
            }

            if (currentUser.UserName == "Guest")
            {
                bool inValidEmail = true;
                while (inValidEmail)
                {
                    Console.WriteLine("Email Adres: ");
                    string emailValidate = Console.ReadLine();


                    // verfify if it is a valid email with a @
                    if (emailRegex.IsMatch(emailValidate))
                    {
                        UserEmail = emailValidate;

                        // end loop
                        inValidEmail = false;
                    }
                    else
                    {
                        Console.WriteLine("Email onjuist, probeer opnieuw");
                    }

                }
            }else if(currentUser.UserName == "Admin")
            {
                UserEmail = CinemaMail;
            }else{
                UserEmail = currentUser.Email;
            }

            Console.WriteLine("Onderwerp: ");
            string Subject = Console.ReadLine();
            Subject = $"Contact Bioscoop: {Subject}";

            Console.WriteLine("Bericht: ");
            string Message = Console.ReadLine();

            string Body = @"
                <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
                <html>
                <head>
                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' >
                    <title>Mailto</title>
                    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet'>
                    <style type='text/css'>
                    html { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;}

                        @media only screen and (min-device-width: 750px) {
                            .table750 {width: 750px !important;}
                        }
                        @media only screen and (max-device-width: 750px), only screen and (max-width: 750px){
                            table[class='table750'] {width: 100% !important;}
                            .mob_b {width: 93% !important; max-width: 93% !important; min-width: 93% !important;}
                            .mob_b1 {width: 100% !important; max-width: 100% !important; min-width: 100% !important;}
                            .mob_left {text-align: left !important;}
                            .mob_center {text-align: center !important;}
                            .mob_soc {width: 50% !important; max-width: 50% !important; min-width: 50% !important;}
                            .mob_menu {width: 50% !important; max-width: 50% !important; min-width: 50% !important; box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2); }
                            .mob_btn {width: 100% !important; max-width: 100% !important; min-width: 100% !important;}
                            .mob_pad {width: 15px !important; max-width: 15px !important; min-width: 15px !important;}
                            .top_pad {height: 15px !important; max-height: 15px !important; min-height: 15px !important;}
                            .top_pad2 {height: 50px !important; max-height: 50px !important; min-height: 50px !important;}
                            .mob_title1 {font-size: 32px !important; line-height: 30px;}
                            .mob_title2 {font-size: 18px !important;}
                            .mob_txt {font-size: 20px !important; line-height: 25px !important;}
                        }
                        @media only screen and (max-device-width: 550px), only screen and (max-width: 550px){
                            .mod_div {display: block !important;}
                        }
                        .table750 {width: 750px;}
                    </style>
                </head>
                <body style='margin: 0; padding: 0;'>

                    <table cellpadding='0' cellspacing='0' border='0' width='100%'
                        style='background: #f5f8fa; min-width: 340px; font-size: 1px; line-height: normal;'>
                        <tr>
                            <td align='center' valign='top'>

                            <table cellpadding='0' cellspacing='0' border='0' width='750' class='table750'
                                style='width: 100%; max-width: 750px; min-width: 340px; background: #f5f8fa;'>
                                <tr>
                                    <td class='mob_pad' width='25' style='width: 25px; max-width: 25px; min-width: 25px;'>&nbsp;</td>
                                    <td align='center' valign='top' style='background: #ffffff;'>

                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'
                                        style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;'>
                                        <tr>
                                            <td align='right' valign='top'>
                                                <div class='top_pad' style='height: 25px; line-height: 25px; font-size: 23px;'>&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                        </table>

                                        <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                        style='width: 88% !important; min-width: 88%; max-width: 88%; margin-top: 25px;'>
                                        <tr>
                                            <td class='mob_left' align='center' valign='top'>
                                                <font class='mob_title1' face='' Source Sans Pro', sans-serif' color='#1a1a1a'
                                                    style='font-size: 32px; font-weight: 300; letter-spacing: -1.5px;'>
                                                    <span class='mob_title1' style='font-family: ' Source Sans Pro', Arial, Tahoma, Geneva,
                                                    sans-serif; color: #1a1a1a; font-size: 32px; font-weight: 300; letter-spacing:
                                                    -1.5px;'>#sub#</span>
                                                </font>
                                                <div style='height: 25px; line-height: 25px; font-size: 23px;'>&nbsp;</div>
                                                <font class='mob_title2' face='' Source Sans Pro', sans-serif' color='#5e5e5e'
                                                    style='font-size: 18px; font-weight: 300; letter-spacing: -1px;'>
                                                    <span class='mob_title2' style='font-family: ' Source Sans Pro', Arial, Tahoma, Geneva,
                                                    sans-serif; color: #5e5e5e; font-size: 18px; font-weight: 300; letter-spacing:
                                                    -1px;'>#msg#</span>
                                                </font>
                                                <div style='height: 38px; line-height: 38px; font-size: 36px;'>&nbsp;</div>
                                                <a href='mailto:#customer#?subject=Antwoord op: #sub#'
                                                    style='display: block; width: 100%; height: 68px; font-family: ' Source Sans Pro',
                                                    Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 18px;
                                                    line-height: 68px; text-decoration: none; white-space: nowrap; font-weight: 600;'>

                                                    <table class='mob_btn' cellpadding='0' cellspacing='0' border='0' width='250'
                                                    style='width: 250px !important; max-width: 250px; min-width: 250px; background: #27cbcc; border-radius: 4px;'>
                                                    <tr>
                                                        <td align='center' valign='middle' height='68'>
                                                            <font face='' Source Sans Pro', sans-serif' color='#ffffff'
                                                                style='font-size: 18px; line-height: 68px; text-decoration: none; white-space: nowrap; font-weight: 600;'>
                                                                <span style='font-family: ' Source Sans Pro', Arial, Verdana, Tahoma,
                                                                Geneva, sans-serif; color: #ffffff; font-size: 18px; line-height: 68px;
                                                                text-decoration: none; white-space: nowrap; font-weight:
                                                                600;'>Antwoord&nbsp;op: #customer#</span>
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </a>

                                                <div class='top_pad2' style='height: 38px; line-height: 78px; font-size: 76px;'>&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                        </table>

                                        <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                        style='width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-bottom: none; border-left: none; border-right: none;'>
                                        <tr>
                                            <td class='mob_left' align='center' valign='top'>
                                                <div style='height: 27px; line-height: 27px; font-size: 25px;'>&nbsp;</div>
                                                <font face='' Source Sans Pro', sans-serif' color='#8c8c8c'
                                                    style='font-size: 17px; line-height: 23px;'>
                                                    <span style='font-family: ' Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color:
                                                    #8c8c8c; font-size: 17px; line-height: 23px;'>Antwoorden op klanten is alleen
                                                    bedoeld voor de klanten service</span>
                                                </font>
                                                <div style='height: 30px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                                            </td>
                                        </tr>
                                        </table>

                                        <table cellpadding='0' cellspacing='0' border='0' width='100%'
                                        style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;'>
                                        <tr>
                                            <td align='center' valign='top'>
                                                <div style='height: 34px; line-height: 34px; font-size: 32px;'>&nbsp;</div>

                                            </td>
                                        </tr>
                                        </table>

                                    </td>
                                    <td class='mob_pad' width='25' style='width: 25px; max-width: 25px; min-width: 25px;'>&nbsp;</td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                </body>

                </html>
                ";

            Body = Body.Replace("#sub#", Subject).Replace("#msg#", Message).Replace("#customer#", UserEmail);

            try
            {
                SendMail(CinemaMail, UserEmail, Body, Subject);
            }
            catch (System.Exception)
            {
                Console.Clear();
                Console.WriteLine("Email versturen mislukt.");
            }
  
        }

        public static void SendMail(string ReceiveMail, string SendMail, string Body, string Subject)
        {
            MailAddress to = new MailAddress(ReceiveMail);
            MailAddress from = new MailAddress(SendMail);
            MailMessage mail = new MailMessage(from, to);

            mail.Subject = Subject;
            mail.Body = Body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;

            smtp.Credentials = new NetworkCredential(
                "projectb.bioscoop@gmail.com", "Pr0j3ctB!");
            smtp.EnableSsl = true;

            smtp.Send(mail);

            string Sending = "Email versturen";

            Random r = new Random();
            for (int i = 0; i <= r.Next(2,5); i++)
            {
                Console.Clear();
                Console.WriteLine(Sending);
                Sending += ".";
                Thread.Sleep(500);
            }
            
            Console.Clear();
            Console.WriteLine("Email verstuurd!");
            Console.WriteLine($"Druk op enter om door terug naar het hoofdscherm te gaan");
            
        }
    }
}