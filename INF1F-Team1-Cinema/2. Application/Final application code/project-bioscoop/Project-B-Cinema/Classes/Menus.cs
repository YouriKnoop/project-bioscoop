﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;

namespace Project_B_Cinema.Classes

{
    class Menus
    {   public static Tuple<String,String> StartScreen() {
            String privileges = "admin";
            String name = "test";            
            Console.WriteLine("Welkom bij CINEMA APP NAME! \n");
            Console.WriteLine("Maak een selectie: \n");
            Console.WriteLine("1. Login \n");
            Console.WriteLine("2. Registreer nieuw account \n");
            Console.WriteLine("3. Ga door als gast \n"); 
            switch (Console.ReadLine()) {
                case "1":
                    Console.WriteLine("Login!");
                    Console.WriteLine("Voer gebruikersnaam in");
                    name = Console.ReadLine();
                    return Tuple.Create(name, privileges);
                case "2":
                    Console.WriteLine("Registreer nieuwe gebruiker");
                    return Tuple.Create("NewUser", "customer");
                case "3":
                    Console.WriteLine("Ga door als gast");
                    return Tuple.Create("Guest", "guest");
                }           
            return StartScreen();
        }
        public static bool ShowMainMenu(Tuple<String,String> user)
        {
            Console.Clear();

            Console.WriteLine($"Welkom {user.Item1} bij de BIOSCOOP APP! \n" +
            "Maak een selectie: \n" +
            $"Account privileges: {user.Item2}\n");

            // TODO: When a user is logged in, this option should not be active.
            Console.WriteLine("Option 1: Registreer nieuwe gebruiker\n");

            // Practical information about the Cinema
            Console.WriteLine("Option 2: Cinema informatie\n");

            // Close App
            Console.WriteLine("Option 0: Sluit menu/App");
            
            Console.WriteLine("\r\nMaak een selectie: ");

            switch (Console.ReadLine())
            {
                case "1": // Register new user
                    AddUserMenu();
                    return true;
                case "2": // Cinema information
                    Console.WriteLine("Je koos Bioscoop informatie. (Not yet implemented!, press any key to continue)");
                    Console.ReadLine(); // testline, otherwise the menu will repeat instantly
                    return true;
                case "0": // Close app
                    return false;
                default: // Default = wrong input.. 
                    Console.WriteLine("Geen geldige keuze! (press any key to continue)");
                    Console.ReadLine(); // testline, otherwise the menu will repeat instantly
                    return true;

            }
        }

        private static void AddUserMenu()
        {
            Console.Clear();

            Console.WriteLine("Option 1: You want to add a User\n");

            Console.Write("\r\nPlease give a username: \n");
            String userName = Console.ReadLine();

            Console.WriteLine("\r\nPlease give a email: \n");
            String userEmail = Console.ReadLine();

            Console.WriteLine($"Hello, {userName}! Je gegeven email is {userEmail}");

            List<User> newUser = new List<User>();

            newUser.Add(new User() { UserName = userName, Email = userEmail });

           
            var jsonString = JsonConvert.SerializeObject(newUser);

            Console.WriteLine(jsonString + "\n");

            dynamic json = JsonConvert.DeserializeObject(jsonString);


            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;

            string path = parent + "/databases/users.json";

            File.AppendAllText(path, jsonString);

        }

        private static void ReadUsers()
        {
            Console.Clear();

            Console.WriteLine("Reading User Database!");

            string pathing = Environment.CurrentDirectory;
            string parent = Directory.GetParent(pathing).Parent.Parent.FullName;

            string path = parent + "/databases/users.json";

            StreamReader jsonFile = new StreamReader(path);

            string json = jsonFile.ReadToEnd();

            //List<UserObject> Users = JsonConvert.DeserializeObject<List<UserObject>>(json);
            //UserObject UserData = UserObject.Parse(File.ReadAllText(path));
            List<object> users = new List<object>(); 
            
            users.Add(JsonConvert.DeserializeObject(json));

            Console.WriteLine(users);

            Console.Write("\r\n Press anything to continue... \n");
            Console.ReadLine();
        }

    }
}
