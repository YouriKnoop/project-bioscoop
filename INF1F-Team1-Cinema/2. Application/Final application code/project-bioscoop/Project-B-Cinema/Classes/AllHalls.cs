﻿using System;
using System.Collections.Generic;
namespace Project_B_Cinema.Classes
{
    public class AllHalls
    {
        public List<Hall> theHalls { get; set; }
        public double[] seatPrices { get; set; }

        public AllHalls()
        {

        }


        public void changeSeatPrices(int toChange, double newPrice)
        {
            seatPrices[toChange] = newPrice;
        }

        public Hall CreateHallFast()    //This function gives returns a hall. User has to provide a string containing the values of all seats, directly after eachother.
        {
            Console.WriteLine("\rVul zaal naam in:");
            string hallName = Console.ReadLine();
            Console.WriteLine("\rVul aantal rijen in:");
            int rows = Int32.Parse(Console.ReadLine());
            Console.WriteLine("\rVul aantal kolommen in:");
            int rowwidth = Int32.Parse(Console.ReadLine());
            Console.WriteLine("\rVul stoel waarden in: ");
            string valuesInString = Console.ReadLine();
            if (valuesInString.Length != rows * rowwidth)
            {
                Console.WriteLine("Waarschuwing! Aantal stoelwaarden komt niet overeen met rijen * kolommen. Sommige stoelen zullen niet correct worden weergeven.");  //You can still continue. Excess values won't be showed. Lack of values will be filled with empty spaces.
            }

            int[] valuesInInt = new int[valuesInString.Length];
            int i = 0;
            foreach(char c in valuesInString.ToCharArray())
            {
                valuesInInt[i] = Int32.Parse("" + c);
                i++;
            }
            Hall newHall = new Hall(rows, rowwidth, valuesInInt, hallName);
            return newHall;
        }

        public void AddHall()   //This function adds a single hall
        {
            Hall newHall = CreateHallFast();
            if (theHalls == null)
            {
                theHalls = new List<Hall>();
            }
            theHalls.Add(newHall);
        }

        public void removeHall()    //This function removes a single hall
        {
            if (hallListCheck())
            {
                Console.WriteLine("Selecteer een zaal om te verwijderen: ");
                int i = 1;
                foreach (Hall currHall in theHalls)
                {
                    Console.WriteLine($"[{i}]: {currHall.name()}");
                    i++;
                }
                int removeThis = Int32.Parse(Console.ReadLine()) - 1;
                theHalls.RemoveAt(removeThis);
                Console.WriteLine($"Klaar! Zaal verwijderd op index {removeThis + 1}");
            } else
            {
                Console.WriteLine("Er zijn geen zalen!");
            }
            
        }

        public void removeAllHalls()    //This function removes all halls in this class
        {
            theHalls.Clear();
        }


        public bool hallListCheck()
        {
            return theHalls != null;
        }

        

        public void resetToDefaultHalls()   //This function resets all halls in this class to the default halls provided by the PO
        {
            theHalls.Clear();
            int[] zaal150values = new int[] {0,0,1,1,1,1,1,1,1,1,0,0,
                                             0,1,1,1,1,1,1,1,1,1,1,0,
                                             0,1,1,1,1,1,1,1,1,1,1,0,
                                             1,1,1,1,1,2,2,1,1,1,1,1,
                                             1,1,1,1,2,2,2,2,1,1,1,1,
                                             1,1,1,2,2,3,3,2,2,1,1,1,
                                             1,1,1,2,2,3,3,2,2,1,1,1,
                                             1,1,1,2,2,3,3,2,2,1,1,1,
                                             1,1,1,2,2,3,3,2,2,1,1,1,
                                             1,1,1,1,2,2,2,2,1,1,1,1,
                                             1,1,1,1,1,2,2,1,1,1,1,1,
                                             0,1,1,1,1,1,1,1,1,1,1,0,
                                             0,0,1,1,1,1,1,1,1,1,0,0,
                                             0,0,1,1,1,1,1,1,1,1,0,0};
            int[] zaal300values = new int[] {0,1,1,1,1,1, 1,1,1,1,1,1, 1,1,1,1,1,0,
                                             0,1,1,1,1,1, 2,2,2,2,2,2, 1,1,1,1,1,0,
                                             0,1,1,1,1,2, 2,2,2,2,2,2, 2,1,1,1,1,0,
                                             0,1,1,1,1,2, 2,2,2,2,2,2, 2,1,1,1,1,0,
                                             0,1,1,1,2,2, 2,2,2,2,2,2, 2,2,1,1,1,0,
                                             0,1,1,1,2,2, 2,2,3,3,2,2, 2,2,1,1,1,0,
                                             1,1,1,2,2,2, 2,3,3,3,3,2, 2,2,2,1,1,1,
                                             1,1,1,2,2,2, 3,3,3,3,3,3, 2,2,2,1,1,1,
                                             1,1,2,2,2,2, 3,3,3,3,3,3, 2,2,2,2,1,1,
                                             1,1,2,2,2,2, 3,3,3,3,3,3, 2,2,2,2,1,1,
                                             1,1,2,2,2,2, 3,3,3,3,3,3, 2,2,2,2,1,1,
                                             0,1,1,2,2,2, 2,3,3,3,3,2, 2,2,2,1,1,0,
                                             0,1,1,1,2,2, 2,2,3,3,2,2, 2,2,1,1,1,0,
                                             0,1,1,1,1,2, 2,2,2,2,2,2, 2,1,1,1,1,0,
                                             0,0,1,1,1,1, 2,2,2,2,2,2, 1,1,1,1,0,0,
                                             0,0,1,1,1,1, 2,2,2,2,2,2, 1,1,1,1,0,0,
                                             0,0,1,1,1,1, 1,1,1,1,1,1, 1,1,1,1,0,0,
                                             0,0,0,1,1,1, 1,1,1,1,1,1, 1,1,1,0,0,0,
                                             0,0,0,1,1,1, 1,1,1,1,1,1, 1,1,1,0,0,0};
            int[] zaal500values = new int[] {0,0,0,0,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,0,0,0,0,
                                             0,0,0,1,1,1,1,1,1,2,2, 2,2,2,2,2,2,2,2, 2,2,1,1,1,1,1,1,0,0,0,
                                             0,0,0,1,1,1,1,1,2,2,2, 2,2,2,2,2,2,2,2, 2,2,2,1,1,1,1,1,0,0,0,
                                             0,0,0,1,1,1,1,1,2,2,2, 2,2,2,2,2,2,2,2, 2,2,2,1,1,1,1,1,0,0,0,
                                             0,0,0,1,1,1,1,2,2,2,2, 2,2,3,3,3,3,2,2, 2,2,2,2,1,1,1,1,0,0,0,
                                             0,0,1,1,1,1,1,2,2,2,2, 2,3,3,3,3,3,3,2, 2,2,2,2,1,1,1,1,1,0,0,
                                             0,1,1,1,1,1,2,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,2,1,1,1,1,1,0,
                                             1,1,1,1,1,1,2,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,2,1,1,1,1,1,1,
                                             1,1,1,1,1,2,2,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,2,2,1,1,1,1,1,
                                             1,1,1,1,1,2,2,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,2,2,1,1,1,1,1,
                                             1,1,1,1,1,1,2,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,2,1,1,1,1,1,1,
                                             1,1,1,1,1,1,1,2,2,2,2, 3,3,3,3,3,3,3,3, 2,2,2,2,1,1,1,1,1,1,1,
                                             0,1,1,1,1,1,1,1,2,2,2, 2,2,3,3,3,3,2,2, 2,2,2,1,1,1,1,1,1,1,0,
                                             0,0,1,1,1,1,1,1,2,2,2, 2,2,2,2,2,2,2,2, 2,2,2,1,1,1,1,1,1,0,0,
                                             0,0,1,1,1,1,1,1,1,2,2, 2,2,2,2,2,2,2,2, 2,2,1,1,1,1,1,1,1,0,0,
                                             0,0,0,1,1,1,1,1,1,1,2, 2,2,2,2,2,2,2,2, 2,1,1,1,1,1,1,1,0,0,0,
                                             0,0,0,1,1,1,1,1,1,1,1, 1,2,2,2,2,2,2,1, 1,1,1,1,1,1,1,1,0,0,0,
                                             0,0,0,0,0,1,1,1,1,1,1, 1,1,1,1,1,1,1,1, 1,1,1,1,1,1,0,0,0,0,0,
                                             0,0,0,0,0,0,0,1,1,1,1, 1,1,1,1,1,1,1,1, 1,1,1,1,0,0,0,0,0,0,0,
                                             0,0,0,0,0,0,0,0,1,1,1, 1,1,1,1,1,1,1,1, 1,1,1,0,0,0,0,0,0,0,0};
            theHalls.Add(new Hall(14, 12, zaal150values, "Zaal150"));
            theHalls.Add(new Hall(19, 18, zaal300values, "Zaal300"));
            theHalls.Add(new Hall(20, 30, zaal500values, "Zaal500"));
        }


        public void printAllHalls()    //This function prints for each hall the hal name and the number of seats
        {
            if (hallListCheck())
            {
                int i = 1;
                foreach (Hall currHall in theHalls)
                {
                    Console.WriteLine($"[{i}]: {currHall.name()}, stoelen: {currHall.getNumberOfSeats()}");
                    i++;
                }
            } else
            {
                Console.WriteLine("Er zijn geen zalen! Maak een zaal aan met 'zaal toevoegen'.");
            }
            
        }

        public void showHallLayout()   //Shows layout of one of the halls
        {

            if (hallListCheck())
            {
                Console.WriteLine("Selecteer een zaal om de layout van te bekijken: ");
                int i = 1;
                foreach (Hall currHall in theHalls)
                {
                    Console.WriteLine($"[{i}]: {currHall.name()}");
                    i++;
                }
                int showThis = Int32.Parse(Console.ReadLine()) - 1;
                if (showThis < theHalls.Count && showThis >= 0)
                {
                    theHalls[showThis].printHall();
                    Console.WriteLine("Regular seats ( . ): €{0:N2}", seatPrices[0]);
                    Console.WriteLine("Viewer seats ( + ): €{0:N2}", seatPrices[1]);
                    Console.WriteLine("Premium seats ( * ): €{0:N2}", seatPrices[2]);
                }
                else
                {
                    Console.WriteLine("Voer alstublieft een geldig stoelnummer in.");
                }
            } else
            {
                Console.WriteLine("Er zijn geen zalen om de layout van te bekijken!");
            }
            
        }

        public void showHallLayout2(int showThis, List<int> mySeats, List<int> reservedSeats)
        {
            Console.WriteLine(theHalls[showThis].printHallWithOccupation2(mySeats, reservedSeats));
            Console.WriteLine("Regular seats ( . ): +€{0:N2}", seatPrices[0]);
            Console.WriteLine("Viewer seats ( + ): +€{0:N2}", seatPrices[1]);
            Console.WriteLine("Premium seats ( * ): +€{0:N2}", seatPrices[2]);
            Console.WriteLine("Bezet (X)");
            Console.WriteLine("Mijn reservering (S)");

        }
        public string showHallLayoutAsString(int showThis, List<int> mySeats, List<int> reservedSeats)
        {
            string s = "";
            s += theHalls[showThis].printHallWithOccupation2(mySeats, reservedSeats);
            s += "Regular seats ( . ): +€" + string.Format("{0:N2}", this.seatPrices[0]) + "\n";
            s += "Viewer seats ( + ): +€" + string.Format("{0:N2}", this.seatPrices[1]) + "\n";
            s += "Premium seats ( * ): +€" + string.Format("{0:N2}", this.seatPrices[2]) + "\n";
            s += ("Bezet (X)\n");
            s += ("Mijn reservering (S)");
            return s;
        }

    }


    public class Hall
    {
        public int[] values { get; set; }    //Array of integers which represent the value of the seat. 0 means there is no seat here.
        public int rows { get; set; }
        public int rowwidth { get; set; }
        public string hallName { get; set; }

        public Hall(int r, int rwidth, int[] vals, string name)
        {
            this.rows = r;
            this.rowwidth = rwidth;
            this.values = vals;
            this.hallName = name;

        }

        public string name()
        {
            return this.hallName;
        }

        public bool[] getReservationTable()
        {
            bool[] occupation = new bool[rows * rowwidth];

            for (int i = 0; i < occupation.Length; i++)
            {
                occupation[i] = false;
            }

            return occupation;
        }


        public void printHall()
        {
            string hallString = "   ";
            int k = 0;
            for (int g = 1; g <= rowwidth; g++)    //This for loop shows the numbers at the top of the hall
            {
                if (g < 10)
                {
                    hallString += g + "  ";
                } else
                {
                    hallString += g + " ";
                }

                
            }
            hallString += "\n";
            for (int i = 0; i < rows; i++)
            {
                if (i + 1 < 10)     //Show row number
                {
                    hallString += $"{i + 1}  ";
                } else if (i < 100)
                {
                    hallString += $"{i + 1} ";
                } else
                {
                    hallString += $"{i + 1}";
                }


                for(int j = 0; j < rowwidth; j++)
                {   if (k >= values.Length)
                    {
                        hallString += " ";    //Add an empty place if there weren't enough values in the array
                    } else
                    {
                        if (values[k] == 0)
                        {
                            hallString += " ";    //No Seat
                        }
                        else if (values[k] == 1)
                        {
                            hallString += ".";    //Lowest value seat (cheapest)
                        }
                        else if (values[k] == 2)
                        {
                            hallString += "+";   //Second value seat
                        }
                        else if (values[k] == 3)
                        {
                            hallString += "*";   //Third value seat
                        }
                        else if (values[k] >= 4)
                        {
                            hallString += "$";   //Fourth value seat (most expensive) (not in the example halls provided by PO)
                        }
                    }
                    hallString += "  ";    //Add two spaces to make numbers at the top possible
                    
                    k++;
                }
                hallString += "\n";
            }
            Console.WriteLine(hallString);
        }

        public int getNumberOfSeats()   //Returns the number of seats in the hall. Doesn't count empty seats
        {
            int nrOfSeats = 0;
            foreach (int val in values)
            {
                if (val != 0)
                {
                    nrOfSeats += 1;
                }
            }
            return nrOfSeats;
        }

        public bool seatIsValid(int seatNumber)   //Checks if a seatnumber is within bounds and if there is a seat at that spot. Seatnumbers start at zero.
        {
            if (seatNumber < values.Length)
            {
                return 0 <= seatNumber && seatNumber < rows * rowwidth && values[seatNumber] != 0;
            }
            else
            {
                return false;
            }
        }

        public string printHallWithOccupation2(List<int> seatNumbers, List<int> reservedSeats)
        {
            string hallString = "   ";

            for (int g = 1; g <= rowwidth; g++)   //Add seat numbers at the top
            {
                if (g < 10)
                {
                    hallString += g + "  ";
                }
                else
                {
                    hallString += g + " ";
                }


            }
            hallString += "\n";
            int k = 0;
            // foreach (int seat in reservedSeats)
            // {
            //     Console.WriteLine(seat.ToString());
            // }
            // Console.ReadLine();
            for (int i = 0; i < rows; i++)
            {

                //This Displays the row numbers
                if (i + 1 < 10)
                {
                    hallString += $"{i + 1}  ";
                }
                else if (i < 100)
                {
                    hallString += $"{i + 1} ";
                }
                else
                {
                    hallString += $"{i + 1}";
                }



                for (int j = 0; j < rowwidth; j++)
                {
                    if (seatNumbers.Contains(k))
                    {
                        hallString += "S";
                    } else
                    {
 
                        if (reservedSeats.Contains(k))
                        {
                            hallString += "X";   //Set X for occupied seats
                        }
                        else
                        {
                            if (k >= values.Length)
                            {
                                hallString += " ";   //If the values array was too short, it will fill the rest with empty spaces (not seats)
                            }
                            else
                            {
                                if (values[k] == 0)
                                {
                                    hallString += " ";   //No Seat
                                }
                                else if (values[k] == 1)
                                {
                                    hallString += ".";   //First value of seat (cheapest)
                                }
                                else if (values[k] == 2)
                                {
                                    hallString += "+";    //Second value of seat
                                }
                                else if (values[k] == 3)
                                {
                                    hallString += "*";    //Third value of seat
                                }
                                else if (values[k] >= 4)
                                {
                                    hallString += "$";    //Fourth value of seat (most expensive) (not used in the examples)
                                }
                            }
                        }
                    }

                    
                    hallString += "  ";

                    k++;
                }
                hallString += "\n";
            }
            hallString += "Scherm: " + string.Join(" ", new string[(rowwidth - 10)]) + string.Join("-", new string[(rowwidth + 11)]) + "\n";
            return hallString;
        }




    }
}
