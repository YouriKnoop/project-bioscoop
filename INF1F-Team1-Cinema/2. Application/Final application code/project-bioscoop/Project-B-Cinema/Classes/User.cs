﻿using System;
using System.Collections.Generic;
using System.Text;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Datahandler;
using System.IO;
using Newtonsoft.Json;

namespace Project_B_Cinema.Classes
{
    public class User
    {
        public String UserID { get; set; }

        public String UserName { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public DateTime Birthday { get; set; }

        public String Password { get; set; }

        public int Role { get; set; }

    }
}
