﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_B_Cinema.Classes
{
    class LoggedUser
    {
        public String UserID { get; set; }

        public String UserName { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public String Password { get; set; }
    }
}
