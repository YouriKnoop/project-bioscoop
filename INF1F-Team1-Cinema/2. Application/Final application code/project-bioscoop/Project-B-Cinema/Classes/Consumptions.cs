﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.Json;

namespace Project_B_Cinema.Classes
{
    class Consumptions
    {
        public int ID { get; set; }

        public string Type { get; set; }

        public string Sort { get; set; }

        public double Price { get; set; }

        public static Tuple<double, List<string>, List<double>> GetConsumptions()
        {
            
            List<string> options = new List<string>();
            List<double> prices = new List<double>();

            List<Consumptions> consumptionList = Datahandler.DataStorageHandler.Storage.Consumptions;
            foreach (Consumptions consumption in consumptionList)
            {
                options.Add($"{consumption.Sort} ({consumption.Price.ToString("C")})");
                prices.Add(consumption.Price);
            }

            // count the list
            int totalOptions = options.Count;

            return SelectConsumptions(totalOptions, options, prices);
        }

        private static Tuple<double, List<string>, List<double>> SelectConsumptions(int totalOptions, List<string> options, List<double> prices)
        {
            ConsoleKey keyPressed = ConsoleKey.Z;
            List<string> order = new List<string>();
            List<double> orderPrices = new List<double>();
            int selectedIndex = 0;
            double lastAdded = 0;
            double totalPrice = 0;

            while (keyPressed != ConsoleKey.D1 && keyPressed != ConsoleKey.D2)
            {
                // reset console
                Console.Clear();
                // write info to console
                ItemHover(totalOptions, options, selectedIndex, order, totalPrice);
                // set key info
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                keyPressed = keyInfo.Key;

                //keypressed actions

                // go to previous item
                if (keyPressed == ConsoleKey.UpArrow)
                {
                    selectedIndex--;
                    if (selectedIndex < 0)
                    {
                        selectedIndex = 0;
                    }
                }
                // go to next item
                else if (keyPressed == ConsoleKey.DownArrow)
                {
                    selectedIndex++;
                    if (selectedIndex >= totalOptions)
                    {
                        selectedIndex = totalOptions - 1;
                    }
                }
                // add selected added item
                else if (keyPressed == ConsoleKey.Spacebar || keyPressed == ConsoleKey.Enter)
                {
                    // save item
                    lastAdded = prices[selectedIndex];

                    // add item
                    order.Add(options[selectedIndex]);
                    orderPrices.Add(prices[selectedIndex]);
                    totalPrice += prices[selectedIndex];
                }
                // remove last added item
                else if (keyPressed == ConsoleKey.Backspace && order.Count > 0)
                {
                    orderPrices.RemoveAt(order.Count - 1);
                    order.RemoveAt(order.Count - 1);
                    totalPrice -= lastAdded;
                }
            }
            // return all info
            return Tuple.Create(totalPrice, order, orderPrices);
        }

        private static void ItemHover(int totalOptions, List<string> options, int selectedIndex, List<string> order, double totalPrice)
        {
            // display order if more then 1 item is chosen
            if (order.Count > 0)
            {
                Console.WriteLine(string.Join("\n", order));
                Console.WriteLine($"Total price: {totalPrice.ToString("C")}");
            }
            else
            {
                Console.WriteLine("er zijn geen consumpties toegevoegd");
            }

            // seperator
            Console.WriteLine("----------------------------------------------");

            // for each item create button
            for (int i = 0; i < totalOptions; i++)
            {
                string currentOption = options[i];

                if (i == selectedIndex)
                {
                    // selected item
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Blue;
                }
                else
                {
                    // other items
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ResetColor();

                }
                Console.WriteLine($"    {currentOption}");
                Console.ResetColor();
            }
            Console.ResetColor();
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine("Klik enter om toe te voegen aan de bestelling");
            Console.WriteLine("Klik backspace om het laatste product te verwijderen");
            Console.WriteLine("1: Doorgaan");
        }

    }
}
