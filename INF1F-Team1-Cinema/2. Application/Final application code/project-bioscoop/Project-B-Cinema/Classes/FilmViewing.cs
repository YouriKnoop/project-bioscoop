﻿using System;
using System.Collections.Generic;
using Project_B_Cinema.Admin;
using Project_B_Cinema.Datahandler;
namespace Project_B_Cinema.Classes
{
    public class FilmViewing
    {
        public DateTime viewingTime { get; set; }
        //public bool[] reservations { get; set; }
        public List<int> reservedSeats {get; set;}
        public int hallNumber { get; set; }
        public string filmName { get; set; }

        public FilmViewing(int hallNumber, DateTime viewingTime, string filmName)
        {
            HallMenu.Update();
            this.hallNumber = hallNumber;
            //this.reservations = HallMenu.AllHallsObject.theHalls[hallNumber].getReservationTable();
            this.viewingTime = viewingTime;
            this.reservedSeats = new List<int>{};
            this.filmName = filmName;
        }
        public void addReservedSeat(int seatNumber) {
            reservedSeats.Add(seatNumber);
        }
        // public void occupySeat(int seatNumber)
        // {
        //     reservations[seatNumber] = true;
        // }

        public int toSeatNumber(int row, int col)
        {
            HallMenu.Update();
            Hall currentHall = HallMenu.AllHallsObject.theHalls[hallNumber];
            int maxrows = currentHall.rows;
            int maxcols = currentHall.rowwidth;
            int seatNumber = -1;
            if (row <= maxrows && col <= maxcols) {
                seatNumber = (row - 1) * maxcols + col - 1;
                return seatNumber;  
            }
            return seatNumber;
        }

        public bool seatIsValid(int seatNumber)
        {
            HallMenu.Update();
            Hall currentHall = HallMenu.AllHallsObject.theHalls[hallNumber];
            //return currentHall.seatIsValid(seatNumber) && reservations[seatNumber] == false;
            return currentHall.seatIsValid(seatNumber);

        }
        public List<int> updateOccupiedSeats() {
            List<int> occupiedSeats = new List<int>{};
            foreach (Reservation reservation in DataStorageHandler.Storage.Reservations)
            {
                if (reservation.filmName == filmName && reservation.filmViewingTime == viewingTime) {
                    foreach (int seat in reservation.seats)
                    {
                        occupiedSeats.Add(seat);
                    }
                }
            }
            return occupiedSeats;
        }

        public double getTruePrice(int seatNumber, Film CurrentFilm)
        {
            Hall currentHall = HallMenu.AllHallsObject.theHalls[hallNumber];
            int seatVal = currentHall.values[seatNumber];
            double price = HallMenu.AllHallsObject.seatPrices[seatVal - 1];
            price += CurrentFilm.price;
            return price;
        }

        public string toSeatString(List<int> seatNumbers, Film CurrentFilm, Tuple<double, List<string>, List<double>> priceandfood)
        {
            string reval = "Film: " + CurrentFilm.filmName + "\nDatum: " + this.viewingTime.ToString("f") + "\n";
            List<int[]> rcseats = new List<int[]>();
            double sum = 0;
            foreach(int seat in seatNumbers)
            {
                rcseats.Add(toRowandCol(seat));
                sum += getTruePrice(seat, CurrentFilm);
            }
            int i = 0;
            foreach (int[] rcseat in rcseats)
            {
                reval += $"Rij: {rcseat[0]}, Kolom: {rcseat[1]}, Prijs: " + getTruePrice(seatNumbers[i++], CurrentFilm).ToString("C") + "\n";
            }
            if (priceandfood.Item2.Count > 0)
            {
                reval += "\nConsumpties:\n";
                sum += priceandfood.Item1;
                foreach (string food in priceandfood.Item2)
                {
                    reval += food + "\n";
                }
            }
            

            reval += "Total: " + sum.ToString("C");

            return reval;
        }



        public int[] toRowandCol(int seatNumber)
        {
            Hall currentHall = HallMenu.AllHallsObject.theHalls[hallNumber];
            int maxrows = currentHall.rows;
            int maxcols = currentHall.rowwidth;
            int c = seatNumber % maxcols + 1;
            int r = (seatNumber - seatNumber % maxcols) / maxcols + 1;
            return new int[] { r, c };
        }

        // public void releaseSeat(int seatNumber)
        // {
        //     reservations[seatNumber] = false;
        // }

        public void printHallWithReservations(List<int> mySeats)
        {
            reservedSeats = updateOccupiedSeats();
            HallMenu.AllHallsObject.showHallLayout2(hallNumber, mySeats, reservedSeats);
        }
        public string printHallWithReservationsToString(List<int> mySeats)
        {   
            string s = "";
            reservedSeats = updateOccupiedSeats();
            s += HallMenu.AllHallsObject.showHallLayoutAsString(hallNumber, mySeats, reservedSeats);
            return s;
        }

        public Reservation[] GetReservations() {
            List<Reservation> reserveringen = new List<Reservation>{};
            foreach (Reservation reservation in DataStorageHandler.Storage.Reservations)
            {
                if (reservation.filmViewingTime == this.viewingTime && reservation.filmName == this.filmName) {
                    reserveringen.Add(reservation);
                }
            }
            Reservation[] listOfReservations = reserveringen.ToArray(); 
            return listOfReservations;
        }
    }
}
