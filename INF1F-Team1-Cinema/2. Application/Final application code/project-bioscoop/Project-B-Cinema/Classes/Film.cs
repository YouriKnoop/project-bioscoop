﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Project_B_Cinema.Admin;

namespace Project_B_Cinema.Classes
{
    public class Film
    {
        public string filmName { get; set; }
        public string filmDescription { get; set; }
        public string actors { get; set; }
        public double price { get; set; }
        public int age { get; set; }
        public string genre { get; set; }
        public int filmDuur { get; set; }
        public List<FilmViewing> viewingList { get; set; }

        public Film(string filmName, string filmDescription, string actors, string genre, int age, double price, int filmduur)
        {
            this.filmName = filmName;
            this.filmDescription = filmDescription;
            this.actors = actors;
            this.genre = genre;
            this.age = age;
            this.price = price;
            this.viewingList = new List<FilmViewing>();
            this.filmDuur = filmduur;
        }

        public void printFilm()
        {
            Console.WriteLine(getFilmString());
            if (viewingList.Count > 0)
            {
                HallMenu.Update();
                CultureInfo.CurrentCulture = new CultureInfo("nl-NL"); //Deze line kan misschien naar een handigere plaats: zet taal naar Nederlands.
                foreach (FilmViewing viewing in viewingList)
                {
                    Console.WriteLine(viewing.viewingTime.ToString("f") + ", Zaal: " + HallMenu.AllHallsObject.theHalls[viewing.hallNumber].hallName);
                }
            }

        }

        public string getFilmString()
        {
            string filmString = "";
            filmString += filmName + "\n";
            filmString += "-------------------------" + "\n";
            filmString += filmDescription + "\n";
            filmString += "Actors: " + actors + "\n";
            filmString += "Age: " + age + "\n";
            filmString += "Genre: " + genre + "\n";
            if (filmDuur > 60)
            {
                if (filmDuur % 60 != 0)
                {
                    filmString += "Speelduur: " + filmDuur / 60 + " uur en " + filmDuur % 60 + " minuten\n";
                } else
                {
                    filmString += "Speelduur: " + filmDuur / 60 + " uur\n";
                }
                
            } else
            {
                filmString += "Speelduur: " + filmDuur + " minuten\n";
            }

            
            filmString += ("Prijs: " + price.ToString("C") + " (exclusief stoel prijs)") + "\n\n";
            return filmString;

        }
    }
}
