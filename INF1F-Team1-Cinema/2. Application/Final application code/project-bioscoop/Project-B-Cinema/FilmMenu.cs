﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Project_B_Cinema.Classes;
using Project_B_Cinema.Admin;
using Project_B_Cinema.Datahandler;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Project_B_Cinema.Pages.Admin;
using System.Threading;

namespace Project_B_Cinema
{
    public class FilmMenu
    {
        static AllFilms AllFilmsObject = new AllFilms();
        // public static void Update()
        // {
        //     saved = true;
        //     CultureInfo.CurrentCulture = new CultureInfo("nl-NL");

        //     string pathing = Environment.CurrentDirectory;
        //     string parent = Directory.GetParent(pathing).Parent.Parent.FullName;
        //     if (pathing.Contains("bin/Debug")) {
        //         path = parent + "/databases/AllFilms.json";
                
        //     }
        //     else {
        //         path = pathing + "/databases/AllFilms.json";
        //     }


        //     if (!(File.Exists(path)))
        //     {
        //         using StreamWriter sw = File.CreateText(path);
        //         Console.WriteLine("File niet gevonden, nieuwe file aangemaakt.");
        //     }

        //     string filecontent = File.ReadAllText(path);

        //     try
        //     {
        //         AllFilmsObject = JsonConvert.DeserializeObject<AllFilms>(filecontent);
        //         if (AllFilmsObject == null)
        //         {
        //             AllFilmsObject = new AllFilms();
        //         }
        //     }
        //     catch (Exception)
        //     {
        //         AllFilmsObject = new AllFilms();
        //     }

        //     foreach(Film film in AllFilmsObject.theFilms)
        //     {
        //         film.viewingList.Sort(CompareViewings);
        //     }

        //     allFilms = DataStorageHandler.Storage.Films;

        // }

        public static void SaveChanges()
        {
            DataStorageHandler.SaveChanges();

            // string result = JsonConvert.SerializeObject(AllFilmsObject);
            // File.WriteAllText(path, result);
        }

        public static void Test()
        {
            AllFilmsObject.theFilms[0].price = 5.50;
            AllFilmsObject.theFilms[1].price = 7.00;
            AllFilmsObject.theFilms[2].price = 3.30;
            AllFilmsObject.theFilms[3].price = 4.99;
            AllFilmsObject.theFilms[4].price = 6.89;
        }


        public FilmMenu()
        {
        }

        public static void run(bool isAdmin, User currentUser)
        {
            // FilmMenu.Update();
            AllFilmsObject.Update();
            bool showMenu = true;
            if (isAdmin == true)
            {
                while (showMenu)
                {
                    showMenu = showFilmMenuAdmin(currentUser);
                }
            } else
            {
                while (showMenu)
                {
                    showMenu = showFilmMenuCustomer(currentUser);
                }
            }
            
        }

        public static int fancyMenu(string[] options, string before, string after)
        {

            ConsoleKey keyPressed = ConsoleKey.Z;
            int selectedIndex = 0;
            while (keyPressed != ConsoleKey.Spacebar && keyPressed != ConsoleKey.Enter)

            {



                Console.Clear();

                if (before != null)
                {
                    Console.WriteLine(before);
                }

                for (int i = 0; i < options.Length; i++)
                {
                    if (selectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    else
                    {
                        Console.ResetColor();
                    }
                    Console.WriteLine(options[i]);
                    Console.ResetColor();
                }

                Console.ResetColor();

                if (after != null)
                {
                    Console.WriteLine(after);
                }


                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                keyPressed = keyInfo.Key;


                if (keyPressed == ConsoleKey.UpArrow)
                {
                    selectedIndex--;
                }
                else if (keyPressed == ConsoleKey.DownArrow)
                {
                    selectedIndex++;
                }

                if (selectedIndex == -1)
                {
                    selectedIndex = options.Length - 1;
                }
                else if (selectedIndex == options.Length)
                {
                    selectedIndex = 0;
                }




            }
            Console.Clear();
            selectedIndex += 1;
            return selectedIndex;
        }

        public static bool showFilmMenuCustomer(User currentUser)
        {
            string[] options = new string[] { "[1] Alle films op tijd en datum", "[2] Alle films alfabetisch", "[3] Terug" };
            int returnedIndex = fancyMenu(options, "Film Menu\n-------------------------", "-------------------------");

            switch (returnedIndex)
            {
                case 1:
                    showFilmDates(currentUser);
                    SaveChanges();
                    return true;
                case 2:
                    showFilms("alphabetic", currentUser);
                    SaveChanges();
                    return true;
                case 3:
                    return false;
            }



            return true;
        }


        public static bool showFilmMenuAdmin(User currentUser)
        {
            string[] options = new string[] { 
            "[1] Voeg film toe", "[2] Verwijder film", "[3] Voeg Film Viewing toe", "[4] Verwijder Film Viewing", "[5] Terug"};
            int returnedIndex = fancyMenu(options, "Film Menu\n-------------------------", "-------------------------");

            switch (returnedIndex)
            {
                /*
                case 1:
                    showFilms("date", currentUser);
                    return true;
                case 2:
                    showFilms("alphabetic", currentUser);
                    return true;
                */
                case 1:
                    AddFilm();
                    SaveChanges();
                    return true;
                case 2:
                    RemoveFilm();
                    SaveChanges();
                    return true;
                case 3:
                    AddFilmViewing();
                    SaveChanges();
                    return true;
                case 4:
                    RemoveFilmViewing();
                    SaveChanges();
                    return true;
                case 5:
                    return false;
            }
            return true;
        }

        public static void showFilmDates(User currentUser)
        {
            List<FilmViewing> filmViewings = new List<FilmViewing>();
            foreach (Film film in AllFilmsObject.theFilms)
            {
                foreach (FilmViewing view in film.viewingList)
                {
                    for(int i = 0; i < 10; i++)
                    {
                        if (view.viewingTime.Day == DateTime.Now.AddDays((double)i).Day)
                            filmViewings.Add(view);
                    }
                    
                }
            }
            filmViewings.Sort((x, y) => DateTime.Compare(x.viewingTime, y.viewingTime));
            ConsoleKey keyPressed = ConsoleKey.Z;
            int selectedIndex = 0;
            while (keyPressed != ConsoleKey.Spacebar && keyPressed != ConsoleKey.Enter)
            {
                for (int i = 0; i < 10; i++)
                {
                    showDay(i, selectedIndex, filmViewings);
                    Console.WriteLine("");
                }

                if (selectedIndex == filmViewings.Count)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                }
                Console.WriteLine("Terug");
                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                keyPressed = keyInfo.Key;


                if (keyPressed == ConsoleKey.UpArrow)
                {
                    selectedIndex--;
                }
                else if (keyPressed == ConsoleKey.DownArrow)
                {
                    selectedIndex++;
                }

                if (selectedIndex == -1)
                {
                    selectedIndex = filmViewings.Count;
                }
                else if (selectedIndex == filmViewings.Count + 1)
                {
                    selectedIndex = 0;
                }
                Console.Clear();

            }
            if (selectedIndex < filmViewings.Count)
            {
                FilmViewing myViewing = filmViewings[selectedIndex];
                Film CurrentFilm = null;
                foreach(Film film in AllFilmsObject.theFilms)
                {
                    if(myViewing.filmName == film.filmName)
                    {
                        CurrentFilm = film;
                    }
                }
                placingReservation(myViewing, CurrentFilm, currentUser);

            }
        }

        public static void placingReservation(FilmViewing myViewing, Film CurrentFilm, User currentUser)
        {
            bool aanHetReserveren = true;
            List<int> mySeats = new List<int>();
            while (aanHetReserveren)
            {
                int newSeat = MakeReservation(myViewing, mySeats);
                if (newSeat != -1)
                {
                    mySeats.Add(newSeat);
                }
                if (mySeats.Count >= 10)
                {
                    break;
                }
                if (mySeats.Count > 0) {
                    int index = fancyMenu(new string[] { "Ja", "Nee" }, "Wil je nog een stoel reserveren?", null);
                    if (index == 2)
                    {
                        aanHetReserveren = false;
                    }
                }

            }
            Tuple<double, List<string>, List<double>> priceandfood = Consumptions.GetConsumptions();

            //check of de user een email heeft
            string UserEmail = "";
            CinemaInfoAdmin adminInfo = CinemaInfoAdmin.loadCinemaInfo();
            string CinemaMail = adminInfo.email;
            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            Console.Clear();

            if (currentUser.UserName == "Guest")
            {
                bool inValidEmail = true;
                while (inValidEmail)
                {
                    Console.WriteLine("Email Adres: ");
                    string emailValidate = Console.ReadLine();


                    // verfify if it is a valid email with a @
                    if (emailRegex.IsMatch(emailValidate))
                    {
                        UserEmail = emailValidate;

                        // end loop
                        inValidEmail = false;
                    }
                    else
                    {
                        Console.WriteLine("Email onjuist, probeer opnieuw");
                    }

                }
            }

            else if (currentUser.UserName == "Admin")
            {
                UserEmail = CinemaMail;
            }

            else
            {
                UserEmail = currentUser.Email;
            }

            int confirm = fancyMenu(new string[] { "Bevestig", "Annuleer" }, "Kloppen de volgende gegevens?\n" + myViewing.toSeatString(mySeats, CurrentFilm, priceandfood) + "\n", null);
            if (confirm == 1)
            {
                Console.WriteLine("Reservering bevestigd.");
                setReservations(mySeats, myViewing, CurrentFilm, currentUser, priceandfood, UserEmail);
                myViewing.reservedSeats = myViewing.updateOccupiedSeats();
                // Console.WriteLine($"Druk op enter om door terug naar het hoofdscherm te gaan");
                Console.ReadKey(false);
            }
            else
            {
                Console.WriteLine("Reservering geannuleerd.");
                Console.WriteLine($"Druk op enter om door terug naar het hoofdscherm te gaan");
                Console.ReadKey(false);
            }
        }

        public static void showDay(int day, int selectedIndex, List<FilmViewing> filmViewings)
        {
            DateTime currentDate = DateTime.Now.AddDays((double)day);
            string filmday = currentDate.ToLongDateString();
            if(day == 0)
            {
                Console.WriteLine("\x1b[1mVandaag\x1b[0m");
            } else if(day == 1)
            {
                Console.WriteLine("\x1b[1mMorgen\x1b[0m");
            }
            else
            {
                Console.WriteLine("\x1b[1m" + filmday + "\x1b[0m");
            }
            for (int i = 0; i < filmViewings.Count; i++)
            {
                if (i == selectedIndex)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                }
                if (filmViewings[i].viewingTime.Day == currentDate.Day)
                {
                    Console.WriteLine("  " + filmViewings[i].viewingTime.ToShortTimeString() + " " + filmViewings[i].filmName);
                }
                Console.ResetColor();
            }
        }


        public static void showFilms(string option, User currentUser)
        {
            int i = 1;
            List<string> filmOptions = new List<string>();
            if (option == "alphabetic")
            {
                AllFilmsObject.theFilms.Sort((x, y) => string.Compare(x.filmName, y.filmName));
            }
            else if (option == "date")
            {
                AllFilmsObject.theFilms.Sort(compareDates);
            }

            foreach (Film film in AllFilmsObject.theFilms)
            {
                filmOptions.Add("[" + i + "] " + film.filmName);
                i++;
            }

            filmOptions.Add("[" + i + "] " + "Terug");
            int returnedIndex = 0;
            if (option == "alphabetic")
            {
                returnedIndex = fancyMenu(filmOptions.ToArray(), "Films alfabetisch\n-------------------------", "-------------------------");
            }
            else if (option == "date")
            {
                returnedIndex = fancyMenu(filmOptions.ToArray(), "Films op datum\n-------------------------", "-------------------------");
            }
            returnedIndex--;
            if (returnedIndex < AllFilmsObject.theFilms.Count)
            {
                Film CurrentFilm = AllFilmsObject.theFilms[returnedIndex];
                if (CurrentFilm.viewingList.Count != 0)
                {
                    List<string> viewingOptions = new List<string>();
                    foreach (FilmViewing view in CurrentFilm.viewingList)
                    {
                        if (view.viewingTime >= DateTime.Now)
                        {
                            viewingOptions.Add(view.viewingTime.ToString("f"));
                        }
                    }
                    viewingOptions.Add("Terug");
                    string filmString = AllFilmsObject.theFilms[returnedIndex].getFilmString();
                    filmString += "Reserveer: ";
                    int viewingIndex = fancyMenu(viewingOptions.ToArray(), filmString, null);
                    viewingIndex--;
                    if (viewingIndex < viewingOptions.Count - 1)
                    {
                        placingReservation(CurrentFilm.viewingList[viewingIndex], CurrentFilm, currentUser);
                    }
                }
                else
                {
                    string filmString = AllFilmsObject.theFilms[returnedIndex].getFilmString();
                    Console.WriteLine(filmString);
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Terug");
                    Console.ResetColor();
                    Console.ReadKey(false);
                }
            }
        }





        public static int compareDates(Film x, Film y)
        {
            if (x.viewingList.Count() == 0)
            {
                if (y.viewingList.Count() == 0)
                {
                    return 0;
                } else
                {
                    return 1;
                }
            } else if (y.viewingList.Count() == 0)
            {
                return -1;
            }
            else
            {
                if (x.viewingList[0].viewingTime < DateTime.Now)
                {
                    if (y.viewingList[0].viewingTime < DateTime.Now)
                    {
                        return DateTime.Compare(x.viewingList[0].viewingTime, y.viewingList[0].viewingTime);
                    } else
                    {
                        return 1;
                    }
                } else if (y.viewingList[0].viewingTime < DateTime.Now)
                {
                    return -1;
                } else
                {
                    return DateTime.Compare(x.viewingList[0].viewingTime, y.viewingList[0].viewingTime);
                }
                
            }
        }

        public static void AddFilm()
        {
            Console.WriteLine("\rFilm Naam: ");
            string filmName = Console.ReadLine();
            Console.WriteLine("\rFilm Beschrijving: ");
            string filmDescription = Console.ReadLine();
            Console.WriteLine("\rActeurs: ");
            string actors = Console.ReadLine();
            Console.WriteLine("\rMinimumleeftijd: ");
            int age;
            try {
                age = Int32.Parse(Console.ReadLine());
            }
            catch {
                Console.WriteLine("Input niet goed! Leeftijd 3 wordt gebruikt.");
                age = 3;
            }
            int filmduur = 0;
            while (filmduur == 0) {
                Console.WriteLine("\rFilmduur in minuten:");
                try {
                    filmduur = Int32.Parse(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Input niet goed, voer aub een geheel getal in.");
                }
            }
            Console.WriteLine("\rGenre: ");
            string genre = Console.ReadLine();
            Console.WriteLine("\rPrijs (gebruik komma als decimaal teken): ");
            string priceString = Console.ReadLine();
            double price;
            while (double.TryParse(priceString, out price) == false)
            {
                Console.WriteLine("Vul alstublieft een nummer in!");
                Console.WriteLine("\rPrijs: ");
                priceString = Console.ReadLine();
            }
            AllFilmsObject.addFilm(filmName, filmDescription, actors, genre, age, price, filmduur);
            AllFilmsObject.theFilms.Sort((x, y) => string.Compare(x.filmName, y.filmName));
            Console.WriteLine("Geulkt! Film is toegevoegd aan de lijst met films.");
            DataStorageHandler.SaveChanges();
        }

        public static void RemoveFilm()
        {
            int i = 1;
            List<string> filmOptions = new List<string>();
            AllFilmsObject.theFilms.Sort((x, y) => string.Compare(x.filmName, y.filmName));
            foreach (Film film in AllFilmsObject.theFilms)
            {
                filmOptions.Add("[" + i + "] " + film.filmName);
                i++;
            }

            filmOptions.Add("[" + i + "] " + "terug");
            int returnedIndex = fancyMenu(filmOptions.ToArray(), "Welke film wil je verwijderen?\n-------------------------", "-------------------------");
            returnedIndex--;
            if (returnedIndex < filmOptions.Count - 1)
            {
                int janee = fancyMenu(new string[] { "Ja", "Nee" }, "Zeker weten dat je deze film wil verwijderen: " + AllFilmsObject.theFilms[returnedIndex].filmName + "?", null);
                janee--;
                if (janee == 0)
                {
                    string filmname = AllFilmsObject.theFilms[returnedIndex].filmName;
                    List<Reservation> reserveringen = DataStorageHandler.Storage.Reservations;
                    List<Reservation> toDelete = new List<Reservation>{};
                    foreach(Reservation reservation in reserveringen) {
                        if (reservation.filmName == filmname) {
                            toDelete.Add(reservation);
                        }
                    }
                    AllFilmsObject.theFilms.RemoveAt(returnedIndex);
                    Console.WriteLine("Film is verwijderd.");
                    Console.WriteLine($"Klik op enter om door te gaan");
                    Console.ReadKey(false);
                    foreach (Reservation reservering in toDelete) {
                        DataStorageHandler.Storage.Reservations.Remove(reservering);
                    }
                    DataStorageHandler.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Verwijderen van film is onderbroken.\nKlik op enter om terug te gaan naar het menu");
                    Console.ReadKey(false);
                }
            }
            
            
        }

        public static void AddFilmViewing()
        {
            int i = 1;
            List<string> filmOptions = new List<string>();
            if (AllFilmsObject.theFilms.Count > 0) {
                AllFilmsObject.theFilms.Sort((x, y) => string.Compare(x.filmName, y.filmName));
                foreach (Film film in AllFilmsObject.theFilms)
                {
                    filmOptions.Add("[" + i + "] " + film.filmName);
                    i++;
                }
            }
            filmOptions.Add("[" + i + "] " + "terug");
            int returnedIndex = fancyMenu(filmOptions.ToArray(), "Films alfabetisch\n-------------------------", "-------------------------");
            if (returnedIndex == i) {
                return;
            }
            returnedIndex--;
            AllFilmsObject.theFilms[returnedIndex].printFilm();
            Console.WriteLine("Voeg datum toe: dd-mm-jaar");
            string dateString = Console.ReadLine() + " ";
            Console.WriteLine("Voeg tijd toe: uu:mm");
            string hourString = Console.ReadLine() + ":00";
            if (hourString.Length == 8)
            {
                dateString += hourString;
            } else
            {
                dateString += "0" + hourString;
            }
            Console.Clear();
            HallMenu.Update();
            HallMenu.AllHallsObject.printAllHalls();
            Console.WriteLine("Voeg zaalnummer toe: ");
            string hallNumberRawImput = Console.ReadLine();
            int hallnumber = Int32.Parse(hallNumberRawImput) - 1;
            try
            {
                var dateTime = DateTime.Parse(dateString);
                Console.Clear();
                Console.WriteLine($"{AllFilmsObject.theFilms[returnedIndex].filmName} op {dateTime} in zaal {hallnumber}");
                AllFilmsObject.theFilms[returnedIndex].viewingList.Add(new FilmViewing(hallnumber, dateTime, AllFilmsObject.theFilms[returnedIndex].filmName));
                AllFilmsObject.theFilms[returnedIndex].viewingList.Sort(CompareViewings);
                Console.WriteLine("Gelukt! Film viewing is toegevoegd.");
            }
            catch (FormatException)
            {
                Console.WriteLine("Kon de input niet omzetten naar DateTime. Zorg ervoor dat je input in het format jaar-maand-dag is en de tijd als uren:minuten.");
            }
            Console.WriteLine($"Klik op enter om door te gaan");
            Console.ReadKey(false);
        }

        public static int CompareViewings(FilmViewing x, FilmViewing y)
        {
            if (x.viewingTime < DateTime.Now)
            {
                if (y.viewingTime < DateTime.Now)
                {
                    return DateTime.Compare(x.viewingTime, y.viewingTime);
                } else
                {
                    return 1;
                }
            } else if (y.viewingTime < DateTime.Now)
            {
                return -1;
            } else
            {
                return DateTime.Compare(x.viewingTime, y.viewingTime);
            }
        }




        public static void RemoveFilmViewing()
        {
            int i = 1;
            List<string> filmOptions = new List<string>();
            AllFilmsObject.theFilms.Sort((x, y) => string.Compare(x.filmName, y.filmName));
            foreach (Film film in AllFilmsObject.theFilms)
            {
                filmOptions.Add("[" + i + "] " + film.filmName);
                i++;
            }

            filmOptions.Add("[" + i + "] " + "terug");
            int returnedIndex = fancyMenu(filmOptions.ToArray(), "Films alfabetisch\n-------------------------", "-------------------------");
            returnedIndex--;
            if (returnedIndex < AllFilmsObject.theFilms.Count)
            {
                Film CurrentFilm = AllFilmsObject.theFilms[returnedIndex];
                if (CurrentFilm.viewingList.Count != 0)
                {
                    List<string> viewingOptions = new List<string>();
                    foreach (FilmViewing view in CurrentFilm.viewingList)
                    {
                        viewingOptions.Add(view.viewingTime.ToString("f"));
                    }
                    viewingOptions.Add("Terug");
                    string filmString = CurrentFilm.getFilmString() + "Verwijder een viewing:";
                    int viewingIndex = fancyMenu(viewingOptions.ToArray(), filmString, null);
                    viewingIndex--;
                    if (viewingIndex < viewingOptions.Count - 1)
                    {
                        Console.WriteLine("Film viewing met datum en tijd " + CurrentFilm.viewingList[viewingIndex].viewingTime.ToString("f") + " is verwijderd.");
                        CurrentFilm.viewingList.RemoveAt(viewingIndex);
                        Console.WriteLine($"Klik op enter om door te gaan");
                        Console.ReadKey(false);
                    }
                }
            }
        }




        public static int MakeReservation(FilmViewing filmViewing, List<int> mySeats)
        {
            if (mySeats.Count < 10) {

                Console.Clear();
                filmViewing.printHallWithReservations(mySeats);
                Console.WriteLine("Vul rij nummer in: ");
                string rowString = Console.ReadLine();
                int row;
                while (int.TryParse(rowString, out row) == false)
                {
                    Console.WriteLine("Vul alstublieft een nummer in!");
                    Console.WriteLine("Vul rij nummer in: ");
                    rowString = Console.ReadLine();
                }
                Console.WriteLine("Vul kolom nummer in: ");
                string columnString = Console.ReadLine();
                int column;
                while (int.TryParse(columnString, out column) == false)
                {
                    Console.WriteLine("Vul alstublieft een nummer in!");
                    Console.WriteLine("Vul kolom nummer in: ");
                    columnString = Console.ReadLine();
                }
                int reval = filmViewing.toSeatNumber(row, column);
                List<int> occupiedSeats = filmViewing.reservedSeats;
                bool adjacentSeat = false;
                if (mySeats.Count() == 0) {
                    adjacentSeat = true;
                }
                foreach (int seat in mySeats) {
                    if (seat == reval +1 || seat == reval -1) {
                        adjacentSeat = true;
                    }
                }
                if (filmViewing.seatIsValid(reval) && occupiedSeats.Contains(reval) == false && adjacentSeat)
                {
                    Console.Clear();
                    Console.WriteLine($"Stoel op rij: {row}, kolom: {column} is nu bezet");
                    Console.WriteLine($"Druk op enter om door te gaan");
                    Console.ReadKey(false);
                    return reval;
                } else
                {
                    Console.Clear();
                    Console.WriteLine("Dit is geen geldige stoel. Stoel is niet gereserveerd.");
                    Console.WriteLine($"Druk op enter om door te gaan");
                    Console.ReadKey(false);
                    return -1;
                }
            }
            else {
                return -1;
            }
            
            
        }


        public static void setReservations(List<int> mySeats, FilmViewing filmViewing, Film CurrentFilm, User currentUser, Tuple<double, List<string>, List<double>> consumptions, string userEmail) //Moet ook nog aan user worden gekoppeld
        {
            if (mySeats.Count != 0)
            {
                double sum = 0;
                foreach (int seat in mySeats)
                {
                    // filmViewing.occupySeat(seat);
                    sum += filmViewing.getTruePrice(seat, CurrentFilm);
                }
                int id;
                if (DataStorageHandler.Storage.Reservations.Count <= 0) {
                    id = 0;
                }
                else {
                    id = DataStorageHandler.Storage.Reservations[DataStorageHandler.Storage.Reservations.Count - 1].id + 1;
                }

                ReservationEmail(mySeats, filmViewing, CurrentFilm, currentUser, consumptions, userEmail, id, sum);

                if(currentUser.Email == null){
                    currentUser.Email = userEmail;
                }

                Reservation reservation = new Reservation{
                    id = id,
                    filmName = CurrentFilm.filmName,
                    filmViewingTime = filmViewing.viewingTime,
                    user = currentUser,
                    seats = mySeats,
                    snacks = consumptions,
                    seatPrice = sum
                };

                DataStorageHandler.Storage.Reservations.Add(reservation);
                filmViewing.updateOccupiedSeats();
                DataStorageHandler.SaveChanges();


                //Sum geeft totale prijs. mySeats is een List<int> met alle stoelnummers. filmViewing en CurrentFilm zijn de filmViewing en CurrentFilm.
            }
            
        }

        public static void ReservationEmail(List<int> mySeats, FilmViewing filmViewing, Film CurrentFilm, User currentUser, Tuple<double, List<string>, List<double>> consumptions, string userEmail, int id, double sum){
        
        CinemaInfoAdmin adminInfo = CinemaInfoAdmin.loadCinemaInfo();
        string CinemaMail = adminInfo.email;
        string subject = $"Reservering voor: {CurrentFilm.filmName}";
        List<int[]> mySeatsRC = new List<int[]>();

        foreach(int seatNumber in mySeats){
            mySeatsRC.Add(filmViewing.toRowandCol(seatNumber));
        }

        string combinedSeats = "";
        foreach (int[] seat in mySeatsRC){
            combinedSeats += $"Rij: {seat[0]}, kolom: {seat[1]}<br>";
        }

        // string combinedSeats = string.Join( "<br>", mySeatsRC);
        string combinedSnacks = string.Join( "<br>", consumptions.Item2);
        var viewingTime = filmViewing.viewingTime.ToString("dd/MM/yyyy HH:mm");
        string name = currentUser.FirstName;

        if (name == null) {
            name = "";
        }

        string Body = @"
            <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
            <html>
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' >
            <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet'>
            <style type='text/css'>
            html { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;}

                @media only screen and (min-device-width: 750px) {
                    .table750 {width: 750px !important;}
                }
                @media only screen and (max-device-width: 750px), only screen and (max-width: 750px){
                    table[class='table750'] {width: 100% !important;}
                    .mob_b {width: 93% !important; max-width: 93% !important; min-width: 93% !important;}
                    .mob_b1 {width: 100% !important; max-width: 100% !important; min-width: 100% !important;}
                    .mob_left {text-align: left !important;}
                    .mob_soc {width: 50% !important; max-width: 50% !important; min-width: 50% !important;}
                    .mob_menu {width: 50% !important; max-width: 50% !important; min-width: 50% !important; box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2); }
                }
                @media only screen and (max-device-width: 700px), only screen and (max-width: 700px){
                    .mob_div {width: 100% !important; max-width: 100% !important; min-width: 100% !important;}
                    .mob_tab {width: 88% !important; max-width: 88% !important; min-width: 88% !important;}
                }
                @media only screen and (max-device-width: 550px), only screen and (max-width: 550px){
                    .mod_div {display: block !important;}
                }
                .table750 {width: 750px;}
            </style>
            </head>
            <body style='margin: 0; padding: 0;'>

            <table cellpadding='0' cellspacing='0' border='0' width='100%'
                style='background: #f3f3f3; min-width: 350px; font-size: 1px; line-height: normal;'>
                <tr>
                    <td align='center' valign='top'>

                        <table cellpadding='0' cellspacing='0' border='0' width='750' class='table750'
                        style='width: 100%; max-width: 750px; min-width: 350px; background: #f3f3f3;'>
                        <tr>
                            <td width='3%' style='width: 3%; max-width: 3%; min-width: 10px;'>&nbsp;</td>
                            <td align='center' valign='top' style='background: #ffffff;'>

                                <table cellpadding='0' cellspacing='0' border='0' width='100%'
                                    style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f3f3f3;'>
                                    <tr>
                                    <td align='right' valign='top'>
                                        <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                    style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                    <tr>
                                    <td align='center' valign='top'>

                                        <div style=' max-width: 233px;'>
                                            <h4 style='text-align: center; font-size: 40px; color: #24d3d6;'> Thanks</h4>
                                        </div>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                    style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                    <tr>
                                    <td align='left' valign='top'>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 28px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>Hey
                                                #name#,</span>
                                        </font>
                                        <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 28px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>Je
                                                reservering voor #Filmname# op #FilmViewingtime# is succesvol afgerond. Hieronder
                                                vind je alle informatie van je reservering.
                                                Ben je iets vergeten of wil je de reservering aanpassen? zoek dan in de app naar je
                                                reservering met de onderstaande code.</span>
                                        </font>

                                        <div style='height: 50px; line-height: 50px; font-size: 48px;'>&nbsp;</div>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                    style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                    <tr>
                                    <td align='left' valign='top'>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px; font-weight: 600;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 26px; font-weight: 600;'>Reservering
                                                nummer:</span>
                                        </font>
                                        <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>

                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>#id#
                                                <hr></span>
                                        </font>
                                    </td>
                                    </tr>

                                    <tr>
                                    <td align='left' valign='top'>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px; font-weight: 600;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 26px; font-weight: 600;'>Gereserveerde
                                                stoel(en):</span>
                                        </font>
                                        <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>

                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>#chairs#
                                                <hr></span>
                                        </font>
                                    </td>
                                    </tr>

                                    <tr>
                                    <td align='left' valign='top'>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px; font-weight: 600;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 26px; font-weight: 600;'>Gereserveerde
                                                snack(s):</span>
                                        </font>
                                        <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>

                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 26px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>#snacks#
                                                <hr></span>
                                        </font>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                    style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                    <tr>
                                    <td align='right' valign='top'>
                                        <div style='height: 14px; line-height: 14px; font-size: 12px;'>&nbsp;</div>
                                        <font face=''Source Sans Pro', sans-serif' color='#000000'
                                            style='font-size: 18px; line-height: 28px; font-weight: bold;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px; font-weight: bold;'>TOTAAL:
                                                #total#</span>
                                        </font>
                                        <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                    style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                    <tr>
                                    <td align='left' valign='top'>

                                        <font face=''Source Sans Pro', sans-serif' color='#333333'
                                            style='font-size: 14px; line-height: 28px;'>
                                            <span
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 18px; line-height: 28px;'>Ging
                                                er iets niet helemaal goed of ging alles juist helemaal perfect? </br>We horen het
                                                graag van je! <a href='mailto:#company#'
                                                style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #24d3d6; font-size: 18px; line-height: 28px; text-decoration: none;'>Klik
                                                hier om ons een mailtje te sturen</a>.</span>
                                        </font>
                                        <div style='height: 50px; line-height: 50px; font-size: 48px;'>&nbsp;</div>
                                    </td>
                                    </tr>
                                </table>

                                <table cellpadding='0' cellspacing='0' border='0' width='100%'
                                    style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f3f3f3;'>
                                    <tr>
                                    <td align='center' valign='top'>
                                        <div style='height: 34px; line-height: 34px; font-size: 32px;'>&nbsp;</div>
                                        <table cellpadding='0' cellspacing='0' border='0' width='88%'
                                            style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                                            <tr>
                                                <td align='center' valign='top'>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    </tr>
                                </table>

                            </td>
                            <td width='3%' style='width: 3%; max-width: 3%; min-width: 10px;'>&nbsp;</td>
                        </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </body>

            </html>
            ";

        Body = Body.Replace("#name#", name).Replace("#Filmname#", CurrentFilm.filmName).Replace("#FilmViewingtime#", viewingTime).Replace("#id#", id.ToString()).Replace("#chairs#", combinedSeats).Replace("#snacks#", combinedSnacks).Replace("#total#", (sum + consumptions.Item1).ToString("c")).Replace("#company#", CinemaMail);
        
        try
            {
                Contact.SendMail(userEmail, CinemaMail, Body, subject);
            }
            catch (System.Exception)
            {
                Console.Clear();
                Console.WriteLine("Email versturen mislukt.");
            }
        }
    }
}
